defmodule SuperTrader.Repo.Migrations.ChangeDataType do
  use Ecto.Migration

  def change do
    alter table(:events) do
      remove :data
      add :data, :map
    end
  end
end
