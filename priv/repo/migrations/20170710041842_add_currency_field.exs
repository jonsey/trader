defmodule SuperTrader.Repo.Migrations.AddCurrencyField do
  use Ecto.Migration

  def change do
    alter table(:events) do
      add :currency, :string
    end
  end
end
