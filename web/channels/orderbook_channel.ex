defmodule SuperTrader.OrderbookChannel do
  use SuperTrader.Web, :channel

  alias SuperTrader.{MarketTradeManager, TradeManager, MarketTradeItem, TimerItem}

  def join("orderbook:lobby", payload, socket) do
    if authorized?(payload) do
      {:ok, socket}
    else
      {:error, %{reason: "unauthorized"}}
    end
  end

  # {rate: '0.00300888', type: 'bid', amount: '3.32349029'}
  def handle_in("orderBookModify", payload, socket) do
    {:reply, {:ok, payload}, socket}
  end

  # {rate: '0.00311164', type: 'ask' }
  def handle_in("orderBookRemove", payload, socket) do
    {:reply, {:ok, payload}, socket}
  end

  # {tradeID: '364476',rate: '0.00300888',amount: '0.03580906',date: '2014-10-07 21:51:20',total: '0.00010775',type: 'sell'}
  def handle_in("newTrade", payload, socket) do
    currency_pair = payload["currency_pair"]
    data = payload["data"]

    market_trade_item = MarketTradeItem.map_trade_data(data, currency_pair)
    MarketTradeManager.receive_trade(:market_trade_manager, market_trade_item)

    {:reply, {:ok, payload}, socket}
  end

  # Add authorization logic here as required.
  defp authorized?(_payload) do
    true
  end
end
