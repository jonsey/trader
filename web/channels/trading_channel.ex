defmodule SuperTrader.TradingChannel do
  use SuperTrader.Web, :channel

  alias SuperTrader.{PredictionTradeManager, TickerManager}
  alias SuperTrader.{Repo}

  def join("trading:lobby", payload, socket) do
    if authorized?(payload) do
      {:ok, socket}
    else
      {:error, %{reason: "unauthorized"}}
    end
  end

  def handle_in("prediction:buy", message, socket) do
    currency = message["payload"]["currency"]
    predictions = message["payload"]["predictions"]

    currency_has_open_trade = SuperTrader.TradingMonitor.currency_has_open_trade(:trading_monitor, currency)

    if currency_has_open_trade == false do
      ticker = TickerManager.get_latest_ticker(:ticker_manager, currency)

      data = %{"currency" => currency, "predictions" => predictions}
      Repo.add_event("predictions:buy", data, currency)

      PredictionTradeManager.buy_trigger_received(ticker.id |> String.to_atom, ticker)
    end

    {:reply, {:ok, message["payload"]}, socket}
  end

  def handle_in("prediction:sell", message, socket) do
    currency = message["payload"]["currency"]
    predictions = message["payload"]["predictions"]

    currency_has_open_trade = SuperTrader.TradingMonitor.currency_has_open_trade(:trading_monitor, currency)

    if currency_has_open_trade do
      ticker = TickerManager.get_latest_ticker(:ticker_manager, currency)

      data = %{"currency" => currency, "predictions" => predictions}
      Repo.add_event("predictions:sell", data, currency)

      PredictionTradeManager.sell_trigger_received(ticker.id |> String.to_atom, ticker)
    end

    {:reply, {:ok, message["payload"]}, socket}
  end

  def handle_in("shout", payload, socket) do
    broadcast socket, "shout", payload
    {:noreply, socket}
  end

  defp authorized?(_payload) do
    true
  end
end
