defmodule SuperTrader.TickerChannel do
  use SuperTrader.Web, :channel

  alias SuperTrader.{TickerManager, TickerItem, ExchangeApi}

  def join("ticker:lobby", payload, socket) do
    if authorized?(payload) do
      # TickerManager.get_all_tickers(:ticker_manager, ExchangeApi.get_all_tickers(:exchange_api))
      {:ok, socket}
    else
      {:error, %{reason: "unauthorized"}}
    end
  end

  def handle_in("tickerReceived", payload, socket) do
    broadcast socket, "tickerReceived", payload
    ticker = TickerItem.map_ticker(payload["ticker"])
    TickerManager.receive_ticker(:ticker_manager, ticker)
    {:reply, {:ok, payload}, socket}
  end

  # Add authorization logic here as required.
  defp authorized?(_payload) do
    true
  end
end
