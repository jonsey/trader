// Brunch automatically concatenates all files in your
// watched paths. Those paths can be configured at
// config.paths.watched in "brunch-config.js".
//
// However, those files will only be executed if
// explicitly imported. The only exception are files
// in vendor, which are never wrapped in imports and
// therefore are always executed.

// Import dependencies
//
// If you no longer want to use a dependency, remember
// to also remove its path from "config.paths.watched".
import "phoenix_html"

// Import local files
//
// Local files can be imported directly using relative
// paths "./socket" or full ones "web/static/js/socket".

import socket from "./socket"
import * as MarketEvents from "./market_events"
// import {drawChart} from "./charts"


// let orderBookChannel = socket.channel("orderbook:lobby", {})
// orderBookChannel.join()
//   .receive("ok", resp => { console.log("Joined successfully", resp) })
//   .receive("error", resp => { console.log("Unable to join", resp) })

let tickerChannel = socket.channel("ticker:lobby", {})
tickerChannel.join()
  .receive("ok", resp => { console.log("Joined ticker lobby successfully", resp) })
  .receive("error", resp => { console.log("Unable to join", resp) })

let tradingChannel = socket.channel("trading:lobby", {})
tradingChannel.join()
  .receive("ok", resp => { console.log("Joined trading lobby successfully", resp) })
  .receive("error", resp => { console.log("Unable to join", resp) })

tradingChannel.on("trade_opened", pl => {
  console.log("trade_opened:", pl);
})

tradingChannel.on("trade_closed:profit", pl => {
  console.log("trade_closed:profit:", pl);
})

tradingChannel.on("trade_closed:loss", pl => {
  console.log("trade_closed:loss:", pl);
})

tradingChannel.on("open_trades", pl => {
  console.log("open_trades:", pl);
})

// tradingChannel.on("ticker_monitor", pl => {
//   const data = [
//     ['Volume' & pl["currency_pair"], pl["volume"]],
//     ['Percentage Change' & pl["currency_pair"], pl["percentage_change"]],
//     ['Base Volume' & pl["currency_pair"], pl["base_volume"]],
//     ['Avg Trade Volume' & pl["currency_pair"], pl["average_trade_volume"]],
//   ]
//   drawChart(data)
// })

// let graph_data_object_array =  [['Currency', '% Change', { role: 'style' }, { role: 'annotation' } ]]
// tradingChannel.on("ticker_monitor", pl => {
//   if(graph_data_object_array.filter(x => { return x[0] === pl["currency_pair"]}).length > 0) {
//     graph_data_object_array.map(x => {
//       if(x[0] === pl["currency_pair"]) {
//         x[1] = pl["percentage_change"]
//         x[2] = pl["percentage_change"] > 0 ? 'green' : 'red'
//       }
//     })
//   } else {
//     graph_data_object_array.push([
//       pl["currency_pair"],
//       pl["percentage_change"],
//       pl["percentage_change"] > 0 ? 'green' : 'red',
//       pl["currency_pair"]
//     ])
//   }
//
//   // const data = google.visualization.arrayToDataTable(graph_data_object_array);
//   drawChart(graph_data_object_array)
// })


var autobahn = require('autobahn');

var predictionWsUri = "ws://localhost:8888/ws";
var predictionWs = new WebSocket(predictionWsUri);
predictionWs.onmessage = function (message) {
  var data = JSON.parse(message.data)
  var event = data.event
  var payload = data.payload
  tradingChannel.push(event, {payload})
}



// ------------------------------------------------------------------------------

var wsuri = "wss://api.poloniex.com";
var connection = new autobahn.Connection({
  url: wsuri,
  realm: "realm1"
});

connection.onopen = function (session) {

	function tickerEvent (args,kwargs) {
    if(args[0].startsWith('BTC_')) {
      const ticker = {
          id : args[0]
        , name : args[0]
        , last : parseFloat(args[1])
        , lowestAsk : parseFloat(args[2])
        , highestBid : parseFloat(args[3])
        , percentChange : parseFloat(args[4])
        , baseVolume : parseFloat(args[5])
        , quoteVolume : parseFloat(args[6])
        , isFrozen : args[7] === 1
        , dayHigh : parseFloat(args[8])
        , dayLow : parseFloat(args[9])
      }

      tickerChannel.push("tickerReceived", {
        ticker
      })
  		// console.log(args[0]);
    }
	}

  session.subscribe('ticker', tickerEvent);

	// session.subscribe('BTC_STR', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "STR"));
  // session.subscribe('BTC_GNT', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "GNT"));
  // session.subscribe('BTC_XRP', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "XRP"));
  // session.subscribe('BTC_ETH', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "ETH"));
  // session.subscribe('BTC_ETC', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "ETC"));
  // session.subscribe('BTC_XEM', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "XEM"));
  // session.subscribe('BTC_DOGE', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "DOGE"));
  // session.subscribe('BTC_LTC', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "LTC"));
  // session.subscribe('BTC_DGB', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "DGB"));
  // session.subscribe('BTC_XMR', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "XMR"));
  // session.subscribe('BTC_SC', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "SC"));
  // session.subscribe('BTC_BTS', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "BTS"));
  // session.subscribe('BTC_DASH', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "DASH"));
  // session.subscribe('BTC_ZEC', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "ZEC"));
  // session.subscribe('BTC_BCN', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "BCN"));
  // session.subscribe('BTC_BTM', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "BTM"));
  // session.subscribe('BTC_FCT', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "FCT"));
  // session.subscribe('BTC_STEEM', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "STEEM"));
  // session.subscribe('BTC_STRAT', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "STRAT"));
  // session.subscribe('BTC_REP', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "REP"));
  // session.subscribe('BTC_LSK', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "LSK"));
  // session.subscribe('BTC_NXT', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "NXT"));
  // session.subscribe('BTC_SYS', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "SYS"));
  // session.subscribe('BTC_ARDR', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "ARDR"));
  // session.subscribe('BTC_MAID', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "MAID"));
  // session.subscribe('BTC_GAME', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "GAME"));
  // session.subscribe('BTC_DCR', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "DCR"));
  // session.subscribe('BTC_GNO', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "GNO"));
  // session.subscribe('BTC_NOTE', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "NOTE"));
  // session.subscribe('BTC_CLAM', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "CLAM"));
  // session.subscribe('BTC_LBC', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "LBC"));
  // session.subscribe('BTC_VTC', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "VTC"));
  // session.subscribe('BTC_BURST', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "BURST"));
  // session.subscribe('BTC_AMP', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "AMP"));
  // session.subscribe('BTC_NAV', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "NAV"));
  // session.subscribe('BTC_SJCX', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "SJCX"));
  // session.subscribe('BTC_RIC', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "RIC"));
  // session.subscribe('BTC_PPC', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "PPC"));
  // session.subscribe('BTC_EXP', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "EXP"));
  // session.subscribe('BTC_PINK', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "PINK"));
  // session.subscribe('BTC_EMC2', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "EMC2"));
  // session.subscribe('BTC_NXC', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "NXC"));
  // session.subscribe('BTC_VIA', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "VIA"));
  // session.subscribe('BTC_POT', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "POT"));
  // session.subscribe('BTC_BTCD', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "BTCD"));
  // session.subscribe('BTC_NEOS', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "NEOS"));
  // session.subscribe('BTC_FLO', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "FLO"));
  // session.subscribe('BTC_RADS', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "RADS"));
  // session.subscribe('BTC_XCP', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "XCP"));
  // session.subscribe('BTC_NAUT', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "NAUT"));
  // session.subscribe('BTC_PASC', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "PASC"));
  // session.subscribe('BTC_BLK', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "BLK"));
  // session.subscribe('BTC_BELA', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "BELA"));
  // session.subscribe('BTC_FLDC', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "FLDC"));
  // session.subscribe('BTC_XPM', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "XPM"));
  // session.subscribe('BTC_NMC', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "NMC"));
  // session.subscribe('BTC_GRC', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "GRC"));
  // session.subscribe('BTC_BCY', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "BCY"));
  // session.subscribe('BTC_XVC', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "XVC"));
  // session.subscribe('BTC_HUC', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "HUC"));
  // session.subscribe('BTC_XBC', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "XBC"));
  // session.subscribe('BTC_VRC', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "VRC"));
  // session.subscribe('BTC_OMNI', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "OMNI"));
  // session.subscribe('BTC_SBD', (args,kwargs) => MarketEvents.MarketEvent(args,kwargs, "SBD"));
}

connection.onclose = function () {
  console.log("Websocket connection closed");
}

connection.open();
