import socket from "./socket"

let orderBookChannel = socket.channel("orderbook:lobby", {})
orderBookChannel.join()
  .receive("ok", resp => { console.log("Joined orderbook lobby successfully", resp) })
  .receive("error", resp => { console.log("Unable to join", resp) })


export function MarketEvent (args,kwargs, currency) {
  args.map((item => {
    orderBookChannel.push(item.type, {
      currency_pair: "BTC_" + currency,
      data: item.data
    })
    // console.log(item.type);
  }))
}
