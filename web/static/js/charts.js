google.charts.load('current', {'packages':['gauge', 'bar']});
// google.charts.setOnLoadCallback(drawChart);

export function drawChart(jsonData) {

  // var data = new google.visualization.DataTable(jsonData);

  var data = google.visualization.arrayToDataTable(jsonData);

  var options = {
        chart: {
          title: 'Latest Percentage Price change'
        },
        hAxis: {
          title: '% Change',
          minValue: 0,
        },
        vAxis: {
          title: 'Currency'
        },
        bars: 'horizontal'
      };

  var chart = new google.charts.Bar(document.getElementById('chart_div'));

  chart.draw(data, options);
}
