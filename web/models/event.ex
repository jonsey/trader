defmodule SuperTrader.Event do
  use SuperTrader.Web, :model

  schema "events" do
    field :type, :string
    field :data, :map
    field :currency, :string
    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:type, :data, :currency])
    |> validate_required([])
  end
end
