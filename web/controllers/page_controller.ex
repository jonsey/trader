defmodule SuperTrader.PageController do
  use SuperTrader.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
