defmodule SuperTrader.TradeValueCalculatorTests do
  use ExUnit.Case, async: true

  setup do
    exchange_response = %{
      "CNOTE" => %{"available" => "10", "btcValue" => "1", "onOrders" => "0.00000000"},
      "OMNI" => %{"available" => "10", "btcValue" => "0.01", "onOrders" => "0.00000000"}
    }
    {:ok, exchange} = SuperTrader.ExchangeApi.start_link(:test)
    {:ok, capital_manager} = SuperTrader.CapitalManager.start_link(:test2)
    {:ok, capital_manager: capital_manager}
  end

  test "Should get trade value", %{capital_manager: capital_manager} do
    trade_value = SuperTrader.CapitalManager.get_trade_value(capital_manager)
    assert trade_value > 0
  end

  test "Should get total account balance", %{capital_manager: capital_manager} do
    balance = SuperTrader.CapitalManager.current_balance(capital_manager)
    assert balance > 0
  end

  test "Should set time of last balance", %{capital_manager: capital_manager} do
    trade_value = SuperTrader.CapitalManager.get_trade_value(capital_manager)
    state = SuperTrader.CapitalManager.get_state(capital_manager)
    assert state.time_of_last_balance != nil
  end
end
