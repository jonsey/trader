defmodule SuperTrader.TickerManagerTest do
  use ExUnit.Case, async: true

  alias SuperTrader.TickerManager

  def send_ticker(manager, index) do
    ticker = %{
       :id => "BTC_STR",
       :name => "BTC_STR",
       :last => index,
       :lowestAsk => 0.0000106,
       :highestBid => 0.0000104,
       :percentChange => 0.025,
       :baseVolume => 6.125,
       :quoteVolume => 245.125,
       :isFrozen => false,
       :dayHigh => 0.0000106,
       :dayLow => 0.0000100,
    }

    # ticker = %{"ticker" => %{"baseVolume" => 2154.10396852, "dayHigh" => 3.2e-5,
    # "dayLow" => 2.11e-5, "highestBid" => 2.997e-5, "id" => "BTC_BTS",
    # "isFrozen" => true, "last" => 3.006e-5, "lowestAsk" => 3.037e-5,
    # "name" => "BTC_BTS", "percentChange" => 0.32715231,
    # "quoteVolume" => 77944529.57206781}}


    TickerManager.receive_ticker(manager, ticker)
  end

  setup do
    {:ok, manager} = SuperTrader.TickerManager.start_link(:test)
    {:ok, manager: manager}
  end

  test "adds the ticker data to state", %{manager: manager} do

    send_ticker(:test, 0.0000105)
    manager_state = TickerManager.get_current_state manager

    assert length(manager_state) == 1
    [h|t] = manager_state
    assert h.last == 0.0000105
  end

  test "only stores 50 tickers in state", %{manager: manager} do
    Enum.map(1..51, fn(x) ->
      SuperTrader.TickerManagerTest.send_ticker(manager, x)
    end)

    manager_state = TickerManager.get_current_state :test

    [oldest | _] = Enum.reverse manager_state
    assert oldest.last == 2
  end

  test "get current price" do
    ticker = %{
       :id => "BTC_STR",
       :name => "BTC_STR",
       :last => 0.0000105,
       :lowestAsk => 0.0000106,
       :highestBid => 0.0000104,
       :percentChange => 0.025,
       :baseVolume => 6.125,
       :quoteVolume => 245.125,
       :isFrozen => false,
       :dayHigh => 0.0000106,
       :dayLow => 0.0000100,
    }

    TickerManager.receive_ticker(:test, ticker)
    latest_ticker = TickerManager.get_latest_ticker(:test, "BTC_STR")
    assert latest_ticker.lowest_ask == 0.0000106
  end

  test "handles missing ticker request" do
    ticker = %{
       :id => "BTC_STR",
       :name => "BTC_STR",
       :last => 0.0000105,
       :lowestAsk => 0.0000106,
       :highestBid => 0.0000104,
       :percentChange => 0.025,
       :baseVolume => 6.125,
       :quoteVolume => 245.125,
       :isFrozen => false,
       :dayHigh => 0.0000106,
       :dayLow => 0.0000100,
    }

    TickerManager.receive_ticker(:test, ticker)
    latest_ticker = TickerManager.get_latest_ticker(:test, "BTC_XRP")
    assert latest_ticker.lowestAsk == 0.0000106
  end

  test "get all tickers" do
    raw_tickers = {:ok, %{"BTC_VRC" => %{"baseVolume" => "32.41978870", "high24hr" => "0.00010647",
     "highestBid" => "0.00009446", "id" => 99, "isFrozen" => "0",
     "last" => "0.00009518", "low24hr" => "0.00009000",
     "lowestAsk" => "0.00009518", "percentChange" => "-0.06704567",
     "quoteVolume" => "327094.69764647"},
   "BTC_GRC" => %{"baseVolume" => "100.46370042", "high24hr" => "0.00001520",
     "highestBid" => "0.00001519", "id" => 40, "isFrozen" => "0",
     "last" => "0.00001520", "low24hr" => "0.00001237",
     "lowestAsk" => "0.00001520", "percentChange" => "0.17738187",
     "quoteVolume" => "7206348.75056247"}}}

     SuperTrader.TickerManager.get_all_tickers(:ticker_manager, raw_tickers)
     state = TickerManager.get_current_state(:ticker_manager)
     assert length(state) == 2
     assert List.first(state).id == BTC_VRC
     assert List.first(state).baseVolume == 32.41978870
  end

end
