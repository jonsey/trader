defmodule SuperTrader.MarketTradeManagerTest do
  use ExUnit.Case, async: true

  alias SuperTrader.{MarketTradeManager, MarketTradeItem}

  setup do
    MarketTradeManager.start_link(:market_trade_manager)
    :ok
  end

  test "should add 60 second volume" do
    trade = %MarketTradeItem{
      currency_pair: "BTC_ETH",
      trade_id: "12",
      rate: 0.01,
      amount: 1,
      date: DateTime.utc_now,
      total: 0.01,
      type: "buy"
    }
    :ok == MarketTradeManager.receive_trade(:market_trade_manager, trade)
    vol = MarketTradeManager.get_volume(:market_trade_manager, "BTC_ETH", 60)

    assert vol.vol == 1
  end

  test "should update 60 second volume" do
    trade1 = %MarketTradeItem{
      currency_pair: "BTC_ETH",
      trade_id: "12",
      rate: 0.01,
      amount: 1,
      date: DateTime.utc_now,
      total: 0.01,
      type: "buy"
    }

    trade2 = %MarketTradeItem{
      currency_pair: "BTC_ETH",
      trade_id: "13",
      rate: 0.01,
      amount: 1,
      date: DateTime.utc_now,
      total: 0.01,
      type: "buy"
    }

    :ok == MarketTradeManager.receive_trade(:market_trade_manager, trade1)
    :ok == MarketTradeManager.receive_trade(:market_trade_manager, trade2)
    vol = MarketTradeManager.get_volume(:market_trade_manager, "BTC_ETH", 60)

    assert vol.vol == 2
  end

  test "should remove vols over 60 mins old" do
    IO.puts "test 3"
    trade1 = %MarketTradeItem{
      currency_pair: "BTC_ETH",
      trade_id: "12",
      rate: 0.01,
      amount: 1,
      date: %DateTime{calendar: Calendar.ISO, day: 22, hour: 8, microsecond: {527771, 6},
            minute: 2, month: 3, second: 25, std_offset: 0, time_zone: "Etc/UTC",
           utc_offset: 0, year: 1418, zone_abbr: "UTC"},
      total: 0.01,
      type: "buy"
    }

    trade2 = %MarketTradeItem{
      currency_pair: "BTC_ETH",
      trade_id: "13",
      rate: 0.01,
      amount: 1,
      date: %DateTime{calendar: Calendar.ISO, day: 22, hour: 8, microsecond: {527771, 6},
            minute: 3, month: 3, second: 25, std_offset: 0, time_zone: "Etc/UTC",
           utc_offset: 0, year: 1418, zone_abbr: "UTC"},
      total: 0.01,
      type: "buy"
    }

    :ok == MarketTradeManager.receive_trade(:market_trade_manager, trade1)
    :ok == MarketTradeManager.receive_trade(:market_trade_manager, trade2)
    vol = MarketTradeManager.get_volume(:market_trade_manager, "BTC_ETH", 60)

    assert vol.vol == 2
  end
end
