defmodule SuperTrader.PriceIncreasingTest do
  use ExUnit.Case, async: true


  test "should be increasing" do
    list = [3,2,1]
    assert is_increasing(list, true)
  end

  defp is_increasing(_, acc) when acc == false do
    false
  end

  defp is_increasing([h|[h1|[]]], acc) when acc do
    h > h1
  end

  defp is_increasing([h|[h1|t]], acc)  do
    is_increasing([h1|t], h > h1)
  end
end
