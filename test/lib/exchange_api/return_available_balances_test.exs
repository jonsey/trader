defmodule SuperTrader.ReturnAvailableBalancesTest do
  use ExUnit.Case, async: true

  setup do
    {:ok, exchange} = SuperTrader.ExchangeApi.start_link(:test)
    {:ok, exchange: exchange}
  end

  test "Should get balances", %{exchange: exchange} do
    {:ok, response} = SuperTrader.ExchangeApi.get_complete_balances(exchange)
    assert response["CNOTE"] != nil
  end
end
