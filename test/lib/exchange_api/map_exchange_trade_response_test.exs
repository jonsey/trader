defmodule SuperTrader.MapExchangeTradeResponseTest do
  use ExUnit.Case, async: true



  setup %{} do
    trade = "{\"amount\":\"338.8732\",\"date\":\"2014-10-18 23:03:21\",\"rate\":\"0.00000173\",\"total\":\"0.00058625\",\"tradeID\":\"16164\",\"type\":\"buy\"}"
    response = {:ok,
                   %HTTPoison.Response{
                      body: "{\"orderNumber\":\"295377907567\",\"resultingTrades\":[#{trade}]}",
                      headers: [{"Date", "Mon, 29 May 2017 22:25:28 GMT"},
                       {"Content-Type", "application/json"}, {"Transfer-Encoding", "chunked"},
                       {"Connection", "keep-alive"},
                       {"Set-Cookie",
                        "__cfduid=d0aceecf7c728cedef03a988538b165041496096727; expires=Tue, 29-May-18 22:25:27 GMT; path=/; domain=.poloniex.com; HttpOnly"},
                       {"Cache-Control", "private"}, {"Server", "cloudflare-nginx"},
                       {"CF-RAY", "366cdea059636a37-LHR"}],
                     status_code: 200
                   }
               }

    {:ok, mapped_response} = SuperTrader.TradeResponseAnalyser.analyse_response(response)
    {:ok, mapped_response: mapped_response, trade: trade}
  end

  test "should return order number", %{mapped_response: mapped_response, trade: trade} do
    assert mapped_response[:orderNumber] == "295377907567"
  end

  test "should return resulting trades", %{mapped_response: mapped_response, trade: trade} do
    assert mapped_response[:resultingTrades] == [trade]
  end

  test "should return success", %{mapped_response: mapped_response, trade: trade} do
    assert mapped_response[:tradeStatus] == {:error, :no_trade}
  end

end
