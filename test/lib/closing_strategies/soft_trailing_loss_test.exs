defmodule SuperTrader.SoftTrailingLossTest do
  use ExUnit.Case, async: true

  alias SuperTrader.ClosingStrategies.SoftTrailingStop
  alias SuperTrader.TickerItem

  test "should increase margin if price has gone up" do
    current_ticker = %TickerItem{ last: 11.00 }
    previous_ticker = %TickerItem{  last: 10.0 }
    stop_loss_margin = 10.00

    stop_loss_value = previous_ticker.last - (previous_ticker.last * (stop_loss_margin / 100.00))

    assert stop_loss_value == 9
    {:ok, value} = SoftTrailingStop.execute(current_ticker, previous_ticker, stop_loss_value, stop_loss_margin)

    assert value == 9.9
  end

  test "should increase margin if price has gone up alot" do
    current_ticker = %TickerItem{ last: 100.00 }
    previous_ticker = %TickerItem{  last: 10.0 }
    stop_loss_margin = 10.00

    stop_loss_value = previous_ticker.last - (previous_ticker.last * (stop_loss_margin / 100.00))

    assert stop_loss_value == 9
    {:ok, value} = SoftTrailingStop.execute(current_ticker, previous_ticker, stop_loss_value, stop_loss_margin)

    assert value == 90
  end

  test "should not change margin if price has gone down" do
    current_ticker = %TickerItem{ last: 9.75 }
    previous_ticker = %TickerItem{  last: 10.0 }
    stop_loss_margin = 5.00

    stop_loss_value = previous_ticker.last - (previous_ticker.last * (stop_loss_margin / 100.00))

    assert stop_loss_value == 9.5
    {:ok, value} = SoftTrailingStop.execute(current_ticker, previous_ticker, stop_loss_value, stop_loss_margin)

    assert value == 9.5
  end

  test "should signal close if if price has gone down below initial margin" do
    current_ticker = %TickerItem{ last: 9.49 }
    previous_ticker = %TickerItem{  last: 10.0 }
    stop_loss_margin = 5.00

    stop_loss_value = previous_ticker.last - (previous_ticker.last * (stop_loss_margin / 100.00))

    assert stop_loss_value == 9.5
    {:close, value} = SoftTrailingStop.execute(current_ticker, previous_ticker, stop_loss_value, stop_loss_margin)

    assert value == 9.5
  end

end
