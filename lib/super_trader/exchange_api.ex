defmodule SuperTrader.ExchangeApi do
  use GenServer

  alias SuperTrader.{TickerManager, TradeManager, PoisonApi, MarketTradeItem, OrderItem}
  # API

  def start_link(name) do
    GenServer.start_link(__MODULE__, :ok, [name: name, timeout: 30000])
  end

  def get_all_tickers(server) do
    GenServer.call(server, {:get_all_tickers})
  end

  def get_highest_bid(server, currency_pair) do
    # TODO: must guard for no ticker returned
    ticker = TickerManager.get_latest_ticker(:ticker_manager, currency_pair)
    ticker.highest_bid
  end

  def get_lowest_ask(server, currency_pair) do
    # TODO: musyt guard for no ticker returned
    ticker = TickerManager.get_latest_ticker(:ticker_manager, currency_pair)
    ticker.lowest_ask
  end

  def return_trade_history(server, currency_pair, start_time, end_time) do
    GenServer.call(server, {:return_trade_history, currency_pair, start_time, end_time})
  end

  def return_open_orders(server, currency_pair) do
    GenServer.call(server, {:return_open_orders, currency_pair}, 30000)
  end

  def return_chart_data(server, currency_pair, period, start_time, end_time) do
    GenServer.call(server, {:return_chart_data, currency_pair, period, start_time, end_time})
  end

  def execute_trade(server, test_or_live, currency_pair, rate, amount, type) do
    GenServer.call(server, {:execute_live_trade, currency_pair, rate, amount, type}, 30000)
  end

  def place_stop_order(server, is_test, currency_pair, rate, amount) do
    GenServer.call(server, {:place_stop_order, currency_pair, rate, amount}, 30000)
  end

  def move_order(server, ordernumber, rate, currency_pair) do
    GenServer.call(server, {:move_order, ordernumber, rate, currency_pair})
  end

  def return_order_trades(server, ordernumber) do
    GenServer.call(server, {:return_order_trades, ordernumber}, 30000)
  end

  def get_balance(server, currency) do
    GenServer.call(server, {:get_balance, currency}, 30000)
  end

  def get_complete_balances(server) do
    GenServer.call(server, {:get_complete_balances}, 30000)
  end

  # def get_available_balances(server) do
  #   url = "https://poloniex.com/tradingApi"
  #   api_key = "F3G2MW94-S04J22IN-ZVPATIKG-YU406ZFB"
  #   secret = "517610e45e8e5c0e8891fc9f7ac9115d32cd249f4c7cee276621789d8221ed1121566f96d262b7ef254530fe0e7212451a16943bc7d177e07ca7dd7d987ebce0"
  #
  #   body = [command: "returnBalances", nonce: DateTime.utc_now |> DateTime.to_unix] |> URI.encode_query
  #
  #   headers = [Key: api_key, Sign: sign(secret, body), "Content-Type": "application/x-www-form-urlencoded"]
  #
  #   balances = case PoisonApi.post(url, body, headers) do
  #     {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
  #       body = body |> Poison.decode!
  #       {:ok, body}
  #     {:error, _} ->
  #       :error
  #     end
  #
  #     {:reply, balances, state}
  # end




  defp sign(key, content) do
    :crypto.hmac(:sha512, key, content)
      |> Base.encode16
  end


  # CALLBACKS

  defp log_request(request) do
    IO.puts "Request made " <> request
    IO.inspect DateTime.utc_now |> DateTime.to_string
  end

  def init(:ok) do
    {:ok, []}
  end

  def handle_call({:get_all_tickers}, _from, state) do
    log_request "get_all_tickers"
    tickers = PoisonApi.get("https://poloniex.com/public?command=returnTicker")
    {:reply, tickers, state}
  end

  def handle_call({:return_open_orders, currency_pair}, _from, state) do
    log_request "return_open_orders"

    url = "https://poloniex.com/tradingApi"
    api_key = "F3G2MW94-S04J22IN-ZVPATIKG-YU406ZFB"
    secret = "517610e45e8e5c0e8891fc9f7ac9115d32cd249f4c7cee276621789d8221ed1121566f96d262b7ef254530fe0e7212451a16943bc7d177e07ca7dd7d987ebce0"

    params = [
      command: "returnOpenOrders",
      currencyPair: currency_pair,
      nonce: System.system_time(:microsecond)
    ]

    body = params |> URI.encode_query

    headers = [Key: api_key, Sign: sign(secret, body), "Content-Type": "application/x-www-form-urlencoded"]

    # IO.inspect body
    orders = case PoisonApi.post(url, body, headers) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        body = body |> Poison.decode!
        {:ok, body}
      {:error, _} ->
        :error
      end

      {:reply, orders, state}
  end

  def handle_call({:place_stop_order, currency_pair, rate, amount}, _from, state) do
    log_request "Place stop order"

    rate = rate * 0.999

    url = "https://poloniex.com/tradingApi"
    api_key = "F3G2MW94-S04J22IN-ZVPATIKG-YU406ZFB"
    secret = "517610e45e8e5c0e8891fc9f7ac9115d32cd249f4c7cee276621789d8221ed1121566f96d262b7ef254530fe0e7212451a16943bc7d177e07ca7dd7d987ebce0"

    params = [
      command: "sell",
      currencyPair: currency_pair,
      rate: rate,
      amount: amount,
      fillOrKill: 1,
      nonce: System.system_time(:microsecond)
    ]

    body = params |> URI.encode_query

    headers = [Key: api_key, Sign: sign(secret, body), "Content-Type": "application/x-www-form-urlencoded"]

    IO.puts("Place stop order request:")
    IO.inspect body

    order_item = case PoisonApi.post(url, body, headers) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        body = body |> Poison.decode!

        cond do
          body["error"] ->
            :error
          true ->
            {:ok, OrderItem.map_data(body, currency_pair)}
          end
      {:error, _} ->
        :error
    end

    {:reply, order_item, state}
  end

  def handle_call({:return_order_trades, order_id}, _from, state) do
    log_request "returnOrderTrades"

    url = "https://poloniex.com/tradingApi"
    api_key = "F3G2MW94-S04J22IN-ZVPATIKG-YU406ZFB"
    secret = "517610e45e8e5c0e8891fc9f7ac9115d32cd249f4c7cee276621789d8221ed1121566f96d262b7ef254530fe0e7212451a16943bc7d177e07ca7dd7d987ebce0"

    params = [
      command: "returnOrderTrades",
      orderNumber: order_id,
      nonce: System.system_time(:microsecond)
    ]

    body = params |> URI.encode_query

    headers = [Key: api_key, Sign: sign(secret, body), "Content-Type": "application/x-www-form-urlencoded"]

    # IO.inspect body
    trades = case PoisonApi.post(url, body, headers) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        body = body |> Poison.decode!
        IO.inspect body
        trades = Enum.map(body, fn(x) ->
          MarketTradeItem.map_trade_data(x)
        end)
        {:ok, trades}
      {:error, _} ->
        :error
      end

      {:reply, trades, state}
  end

  def handle_call({:move_order, ordernumber, rate, currency_pair}, _from, state) do
    log_request "move_order"

    url = "https://poloniex.com/tradingApi"
    api_key = "F3G2MW94-S04J22IN-ZVPATIKG-YU406ZFB"
    secret = "517610e45e8e5c0e8891fc9f7ac9115d32cd249f4c7cee276621789d8221ed1121566f96d262b7ef254530fe0e7212451a16943bc7d177e07ca7dd7d987ebce0"

    params = [
      command: "moveOrder",
      orderNumber: ordernumber,
      rate: rate,
      postOnly: 1,
      nonce: System.system_time(:microsecond)
    ]

    body = params |> URI.encode_query

    headers = [Key: api_key, Sign: sign(secret, body), "Content-Type": "application/x-www-form-urlencoded"]

    IO.puts "Move order response"
    IO.inspect body

    order_item = case PoisonApi.post(url, body, headers) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        body = body |> Poison.decode!
        case body["success"] == 1 do
          true ->
            {:ok, OrderItem.map_data(body, currency_pair)}
          false ->
            :error
        end
      {:error, _} ->
        :error
      end

      {:reply, order_item, state}
  end

  def handle_call({:return_chart_data, currency_pair, period, start_time, end_time}, _from, state) do
    log_request "return_chart_data"
    chart_data = PoisonApi.get("https://poloniex.com/public?command=returnChartData&currencyPair=#{currency_pair}&start=#{start_time}&end=#{end_time}&period=#{period}")
    {:reply, chart_data, state}
  end

  def handle_call({:return_trade_history, currency_pair, start_time, end_time}, _from, state) do
    log_request "return_trade_history" <> currency_pair
    trade_data_response = PoisonApi.get("https://poloniex.com/public?command=returnTradeHistory&currencyPair=#{currency_pair}&start=#{start_time}&end=#{end_time}")
    # IO.inspect trade_data_response
    trade_data = case trade_data_response do
      {:ok, data} ->
        trades = Enum.map(data, fn(x) -> MarketTradeItem.map_trade_data(x, currency_pair) end)
        {:ok, trades}
      _ ->
        :error
    end
    :timer.sleep(200)
    {:reply, trade_data, state}
  end

  def handle_call({:execute_live_trade, currency_pair, rate, amount, type}, _from, state) do
    log_request "execute_live_trade"
    url = "https://poloniex.com/tradingApi"
    api_key = "F3G2MW94-S04J22IN-ZVPATIKG-YU406ZFB"
    secret = "517610e45e8e5c0e8891fc9f7ac9115d32cd249f4c7cee276621789d8221ed1121566f96d262b7ef254530fe0e7212451a16943bc7d177e07ca7dd7d987ebce0"

    params = [
      command: type,
      currencyPair: currency_pair,
      rate: rate,
      amount: amount,
      fillOrKill: 1,
      nonce: System.system_time(:microsecond)
    ]

    body = params |> URI.encode_query

    headers = [Key: api_key, Sign: sign(secret, body), "Content-Type": "application/x-www-form-urlencoded"]

    IO.puts "Execute live trade request"
    IO.inspect body

    response = PoisonApi.post(url, body, headers)

    IO.puts "Execute live trade response"
    IO.inspect response

    order_item = case response do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        body = body |> Poison.decode!

        cond do
          body["error"] ->
            :error
          true ->
            {:ok, OrderItem.map_data(body, currency_pair)}
          end
      {:error, _} ->
        :error
    end

    {:reply, order_item, state}
  end

  #{"orderNumber":31226040,"resultingTrades":[{"amount":"338.8732","date":"2014-10-18 23:03:21","rate":"0.00000173","total":"0.00058625","tradeID":"16164","type":"buy"}]}
  def handle_call({:execute_test_trade, currency_pair, rate, amount, type}, _from, state) do
    trade_time = DateTime.utc_now |> DateTime.to_iso8601
    order_item = %OrderItem{
      order_number: 99999999,
      currency_pair: currency_pair,
      resulting_trades: [%MarketTradeItem{
        amount: amount,
        currency_pair: currency_pair,
        date: trade_time,
        rate: rate,
        total: amount * rate,
        trade_id: "99999",
        type: type
        }]
      }
    IO.puts("Test Trade executed:")
    IO.inspect order_item
    {:reply, order_item, state}
  end

  def handle_call({:get_balance, currency}, _from, state) do
    log_request "get_balance"

    url = "https://poloniex.com/tradingApi"
    api_key = "F3G2MW94-S04J22IN-ZVPATIKG-YU406ZFB"
    secret = "517610e45e8e5c0e8891fc9f7ac9115d32cd249f4c7cee276621789d8221ed1121566f96d262b7ef254530fe0e7212451a16943bc7d177e07ca7dd7d987ebce0"

    params = [
      command: "returnBalances",
      nonce: System.system_time(:microsecond)
    ]

    body = params |> URI.encode_query
    headers = [Key: api_key, Sign: sign(secret, body), "Content-Type": "application/x-www-form-urlencoded"]

    # IO.inspect body
    balance = case PoisonApi.post(url, body, headers) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        body = body |> Poison.decode!

        cond do
          body["error"] ->
            :error
          true ->
            balance = Map.get(body, currency) |> Float.parse
            {:ok, balance}
          end
      {:error, _} ->
        :error
    end

    {:reply, balance, state}
  end

  def handle_call({:get_complete_balances}, _from, state) do
    url = "https://poloniex.com/tradingApi"
    api_key = "F3G2MW94-S04J22IN-ZVPATIKG-YU406ZFB"
    secret = "517610e45e8e5c0e8891fc9f7ac9115d32cd249f4c7cee276621789d8221ed1121566f96d262b7ef254530fe0e7212451a16943bc7d177e07ca7dd7d987ebce0"

    params = [command: "returnCompleteBalances", nonce: System.system_time(:microsecond)]
    body = params |> URI.encode_query

    headers = [Key: api_key, Sign: sign(secret, body), "Content-Type": "application/x-www-form-urlencoded"]

    balances = case PoisonApi.post(url, body, headers) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        body = body |> Poison.decode!
        {:ok, body}
      {:error, _} ->
        :error
      end

      {:reply, balances, state}
  end

  def handle_info(:timeout, state) do
    IO.puts "Exchange API timeout"
    { :noreply, state }
  end
end
