defmodule SuperTrader.TradingMonitor do
  use GenServer
  alias SuperTrader.{Repo}
  # API

  def start_link(name) do
    GenServer.start_link(__MODULE__, :ok, [name: name])
  end

  def trade_opened(server, trade, restoring) do
    GenServer.call(server, {:trade_opened, trade, restoring})
  end

  def trade_closed(server, trade, restoring) do
    GenServer.call(server, {:trade_closed, trade, restoring})
  end

  def can_open_trade(server) do
    GenServer.call(server, {:can_open_trade})
  end

  def currency_has_open_trade(server, currency) do
    GenServer.call(server, {:currency_has_open_trade, currency})
  end

  # Callbacks

  def init(:ok) do
    {:ok, %{
        no_of_open_trades: 0,
        no_of_trades_allowed: 1,
        trades_opened: [],
        trades_closed: [],
        open_trades: []
      }}
  end

  def handle_call({:trade_opened, trade, restoring}, _from,  state) when restoring do
    IO.puts "Open trades at start of Trade Opened = #{state.no_of_open_trades}"
    state = state
      |> Map.put(:no_of_open_trades, state.no_of_open_trades + 1)
      |> add_trade_to_opened_list(trade)
      |> add_trade_to_open_trade_list(trade)

    IO.puts "Open trades at end = #{state.no_of_open_trades}"
    IO.inspect state
    {:reply, state, state}
  end

  def handle_call({:trade_opened, trade, restoring}, _from,  state) do
    IO.puts "Open trades at start of Trade Opened = #{state.no_of_open_trades}"
    state = state
      |> Map.put(:no_of_open_trades, state.no_of_open_trades + 1)
      |> add_trade_to_opened_list(trade)
      |> add_trade_to_open_trade_list(trade)
      |> record_trade_opened(trade)

    IO.puts "Open trades at start of Trade Opened = #{state.no_of_open_trades}"
    IO.inspect state
    {:reply, state, state}
  end

  def handle_call({:trade_closed, trade, restoring}, _from,  state) when restoring do
    IO.puts "Open trades at start of Trade Closed = #{state.no_of_open_trades}"
    state = state
      |> Map.put(:no_of_open_trades, state.no_of_open_trades - 1)
      |> add_trade_to_closed_list(trade)
      |> remove_trade_from_open_trades_list(trade)

    IO.puts "Open trades at end of Trade Closed = #{state.no_of_open_trades}"
    IO.inspect state
    {:reply, state, state}
  end

  def handle_call({:trade_closed, trade, restoring}, _from,  state) do
    IO.puts "Open trades at start of Trade Closed = #{state.no_of_open_trades}"
    state = state
      |> Map.put(:no_of_open_trades, state.no_of_open_trades - 1)
      |> add_trade_to_closed_list(trade)
      |> remove_trade_from_open_trades_list(trade)
      |> record_trade_closed(trade)

    IO.puts "Open trades at end of Trade Closed = #{state.no_of_open_trades}"
    IO.inspect state
    {:reply, state, state}
  end

  def handle_call({:can_open_trade}, _from, state) do
    result = state.no_of_open_trades < state.no_of_trades_allowed
    {:reply, result, state}
  end

  def handle_call({:currency_has_open_trade, currency}, _from, state) do
    open_trades = Enum.filter(state.open_trades, fn(trade) ->
      trade.currency_pair == currency
    end)
    {:reply, length(open_trades) > 0, state}
  end

  defp add_trade_to_opened_list(state, trade) do
    Map.put(state, :trades_opened, [trade | state.trades_opened])
  end

  defp add_trade_to_open_trade_list(state, trade) do
    Map.put(state, :open_trades, [trade | state.open_trades])
  end

  defp add_trade_to_closed_list(state, trade) do
    Map.put(state, :trades_closed, [trade | state.trades_closed])
  end

  defp remove_trade_from_open_trades_list(state, trade) do
    open_trades = Enum.filter(state.open_trades, fn(x) -> x.currency_pair != trade.currency_pair end) # Works because always closing full trade
    Map.put(state, :open_trades, open_trades)
  end

  defp broadcast_open_trades(state) do
    Enum.each(state.open_trades, fn(trade) ->
      SuperTrader.Endpoint.broadcast "trading:lobby", "open_trades", trade
    end)
    state
  end

  defp record_trade_opened(state, trade) do
    data = %{"trade" => trade}
    Repo.add_event("trade:opened", data, trade.currency_pair)
    state
  end

  defp record_trade_closed(state, trade) do
    data = %{"trade" => trade}
    Repo.add_event("trade:closed", data, trade.currency_pair)
    state
  end
end
