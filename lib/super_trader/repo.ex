defmodule SuperTrader.Repo do
  use Ecto.Repo, otp_app: :super_trader
  alias SuperTrader.Event
  import Ecto.Query

  def add_event(type, data, currency) do
    event_changeset = Event.changeset(%Event{}, %{
        type: type,
        data: data,
        currency: currency
      })
    {:ok, %Event{}=event} = insert(event_changeset)
  end

  def load_state(realm, currency) when realm == :trades do
    query = from event in Event,
      where: event.type == "trade:opened" or event.type == "trade:closed",
      where: event.currency == ^currency,
      order_by: event.id,
      select: event

    all(query)
  end

  def load_tickers(currency, from_date) do
    query = from event in Event,
      where: event.type == "ticker",
      where: event.currency == ^currency,
      where: event.inserted_at >= ^from_date,
      order_by: event.id,
      select: event.data

    all(query)
    |> Enum.map(fn(item) -> map_to_ticker(item["ticker"]) end)
  end

  defp map_to_ticker(data) do
    {:ok, date, _} = data["date"] |> DateTime.from_iso8601

    %SuperTrader.TickerItem{
       id: data["id"],
       date: date,
       last: data["last"],
       lowest_ask: data["lowest_ask"],
       highest_bid: data["highest_bid"],
       percent_change: data["percent_change"],
       base_volume: data["base_volume"],
       quote_volume: data["quote_volume"],
       is_frozen: data["is_frozen"] == 1,
       day_high: data["day_high"],
       day_low: data["day_low"],
    }
  end
end
