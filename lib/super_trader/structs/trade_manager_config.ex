defmodule SuperTrader.TradeManagerConfig do

  defstruct [
    limit: nil,
    stop: nil,
    roc_threshold: nil,
    roc_period: nil
  ]

end
