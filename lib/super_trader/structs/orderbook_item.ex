defmodule SuperTrader.OrderbookItem do
  defstruct [
    rate: nil,
    type: nil,
    amount: nil,
    seq: nil,
    update_type: nil
  ]
end
