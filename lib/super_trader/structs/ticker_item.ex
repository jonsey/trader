defmodule SuperTrader.TickerItem do

  alias SuperTrader.{DateTimeUtils}

  defstruct [
    id: nil,
    date: nil,
    last: nil,
    lowest_ask: nil,
    highest_bid: nil,
    percent_change: nil,
    base_volume: nil,
    quote_volume: nil,
    is_frozen: nil,
    day_high: nil,
    day_low: nil
  ]

  def map_ticker(data) do
    %SuperTrader.TickerItem{
       id: data["id"],
       date: DateTime.utc_now,
       last: data["last"],
       lowest_ask: data["lowestAsk"],
       highest_bid: data["highestBid"],
       percent_change: data["percentChange"],
       base_volume: data["baseVolume"],
       quote_volume: data["quoteVolume"],
       is_frozen: data["isFrozen"] == 1,
       day_high: data["high24hr"],
       day_low: data["low24hr"],
    }
  end

  def map_ticker(:bitrex, data) do
    date = DateTimeUtils.from_bitrex data["TimeStamp"]

    %SuperTrader.TickerItem{
       id: data["MarketName"],
       date: date,
       last: data["Last"],
       lowest_ask: data["Ask"],
       highest_bid: data["Bid"],
       percent_change: nil,
       base_volume: data["BaseVolume"],
       quote_volume: data["Volume"],
       is_frozen: false,
       day_high: data["High"],
       day_low: data["Low"],
    }
  end

  def map_ticker(currency_pair, data) do
    %SuperTrader.TickerItem{
       id: currency_pair,
       date: DateTime.utc_now,
       last: data["last"],
       lowest_ask: data["lowestAsk"],
       highest_bid: data["highestBid"],
       percent_change: data["percentChange"],
       base_volume: data["baseVolume"],
       quote_volume: data["quoteVolume"],
       is_frozen: data["isFrozen"] == 1,
       day_high: data["high24hr"],
       day_low: data["low24hr"],
    }
  end
end
