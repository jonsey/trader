defmodule SuperTrader.MarketTradeItem do
  alias SuperTrader.DateTimeUtils

  defstruct [
    currency_pair: nil,
    trade_id: nil,
    rate: nil,
    amount: nil,
    date: nil,
    total: nil,
    type: nil,
    stop_loss_value: nil
  ]

  def map_trade_data(:bitrex, data, currency_pair) do
    date = DateTimeUtils.from_bitrex data["TimeStamp"]
    {rate, _} = data["Price"] |> Float.parse
    {amount, _} = data["Quantity"] |> Float.parse
    {total, _} = data["Total"] |> Float.parse

    %SuperTrader.MarketTradeItem{
      currency_pair: currency_pair,
      trade_id: data["Id"],
      rate: rate,
      amount: amount,
      date: date,
      total: total,
      type: data["OrderType"] |> String.downcase,
      stop_loss_value: nil
    }
  end

  def map_trade_data(data) do
    date = DateTimeUtils.from_poloniex data["date"]
    {rate, _} = data["rate"] |> Float.parse
    {amount, _} = data["amount"] |> Float.parse
    {total, _} = data["total"] |> Float.parse

    %SuperTrader.MarketTradeItem{
      currency_pair: data["currencyPair"],
      trade_id: data["tradeID"],
      rate: rate,
      amount: amount,
      date: date,
      total: total,
      type: data["type"],
      stop_loss_value: nil
    }
  end

  def map_trade_data(data, currency_pair) do
    date = DateTimeUtils.from_poloniex data["date"]
    {rate, _} = data["rate"] |> Float.parse
    {amount, _} = data["amount"] |> Float.parse
    {total, _} = data["total"] |> Float.parse

    %SuperTrader.MarketTradeItem{
      currency_pair: currency_pair,
      trade_id: data["tradeID"],
      rate: rate,
      amount: amount,
      date: date,
      total: total,
      type: data["type"],
      stop_loss_value: nil
    }
  end

  def map_trade_data_from_map(data, currency_pair) do
    date = DateTimeUtils.from_poloniex data.date
    {rate, _} = data.rate |> Float.parse
    {amount, _} = data.amount |> Float.parse
    {total, _} = data.total |> Float.parse

    %SuperTrader.MarketTradeItem{
      currency_pair: currency_pair,
      trade_id: data.tradeID,
      rate: rate,
      amount: amount,
      date: date,
      total: total,
      type: data.type,
      stop_loss_value: nil
    }
  end
end
