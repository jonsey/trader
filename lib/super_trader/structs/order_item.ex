defmodule SuperTrader.OrderItem do

  alias SuperTrader.{DateTimeUtils, MarketTradeItem, OrderItem}

  defstruct [:order_number, :currency_pair, :resulting_trades]

  def map_data(data, currency_pair) do
    IO.inspect data
    resulting_trades = Enum.map(data["resultingTrades"], fn(x) ->
      MarketTradeItem.map_trade_data(x, currency_pair)
    end)

    %OrderItem{
      order_number: data["orderNumber"],
      currency_pair: currency_pair,
      resulting_trades: resulting_trades
      }
  end

  def map_data_bitrex(data) do
    amount =  data["Quantity"] - data["QuantityRemaining"]

    trades = case data["QuantityRemaining"] < data["Quantity"] do
      true ->
        [%MarketTradeItem{
          currency_pair: data["Exchange"],
          trade_id: nil,
          rate: data["PricePerUnit"],
          amount: amount,
          date: DateTimeUtils.from_bitrex(data["Opened"]),
          total: data["Price"],
          type: data["Type"] |> String.downcase
        }]
      false ->
        []
    end

    %OrderItem{
      order_number: data["OrderUuid"],
      currency_pair: data["Exchange"],
      resulting_trades: trades
    }
  end
end
