defmodule SuperTrader.PrePredictionTradeManager.StateLoader do
  alias SuperTrader.{Repo}

  def load_state(state, currency, add_trade_to_state, remove_trade_from_state) do
    if state.state_loaded == false do
      trade_history = Repo.load_state(:trades, currency)
      state = state
        |> apply_history(trade_history, add_trade_to_state, remove_trade_from_state)
        |> Map.put(:state_loaded, true)

      IO.puts("State loaded for #{currency}")
      IO.inspect state
    end

    {:ok, state}
  end

  defp apply_history(state, trade_history, add_trade_to_state, remove_trade_from_state) do
    Enum.reduce(trade_history, state, fn(event, acc) ->
      {:ok, new_state} = apply_history_item(acc, event, event.type, add_trade_to_state, remove_trade_from_state)
      acc = new_state
    end)
  end

  defp apply_history_item(state, event, type, add_trade_to_state, _) when type == "trade:opened" do
    trade = map_trade event.data["trade"]

    state = state
      |> Map.put(:trade_open, true)
      |> add_trade_to_state.(trade)

    SuperTrader.Endpoint.broadcast "trading:lobby", "trade_opened", trade
    SuperTrader.TradingMonitor.trade_opened(:trading_monitor, trade, true)

    {:ok, state}
  end

  defp apply_history_item(state, event, type, _, remove_trade_from_state) when type == "trade:closed" do
    trade = trade = map_trade event.data["trade"]

    state = state
      |> Map.put(:trade_open, false)
      |> Map.put(:stop_order_filled, true)
      |> remove_trade_from_state.(trade)

    SuperTrader.Endpoint.broadcast "trading:lobby", "trade_closed", trade
    SuperTrader.TradingMonitor.trade_closed(:trading_monitor, trade, true)

    {:ok, state}
  end

  defp map_trade(trade) do
    %SuperTrader.MarketTradeItem{
              currency_pair: trade["currency_pair"],
              trade_id: trade["trade_id"],
              rate: trade["rate"],
              amount: trade["amount"],
              date: trade["date"],
              total: trade["total"],
              type: trade["type"],
              stop_loss_value: trade["stop_loss_value"]
            }
  end

end
