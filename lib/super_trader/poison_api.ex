defmodule SuperTrader.PoisonApi do
  def get(url, headers \\ [], options \\ []) do
    url
      |> HTTPoison.get(headers, options)
      |> handle_json
  end

  def post(url, body, headers) do
    response = HTTPoison.post(url, body, headers, [timeout: 30000, recv_timeout: 30000])
    # if String.contains?(body, "returnBalances") == false do
    #     IO.puts "Response from API"
    #     IO.inspect response
    # end

    response
  end

  defp handle_json({:ok, %{status_code: 200, body: body}}) do
    {:ok, Poison.Parser.parse!(body)}
  end

  defp handle_json({_, %{status_code: _, body: body}}) do
    IO.puts "Something went wrong. Please check your internet
             connection"
    IO.inspect body
  end

  defp handle_json({:error, %{id: _, reason: reason}}) do
    IO.puts "Something went wrong. Please check your internet
             connection"
    IO.inspect reason
  end
end
