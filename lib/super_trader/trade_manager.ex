defmodule SuperTrader.TradeManager do
  use GenServer

  alias SuperTrader.{ExchangeApi, TradeValueCalculator, OrderItem}
  alias SuperTrader.ClosingStrategies.{SoftTrailingStop, StopLimit}

  @min_balance 0.015

  def start_link(name, config) do
    GenServer.start_link(__MODULE__, config, [name: name, timeout: 30000])
  end

  def ticker_received(server, ticker, tickers) do
    GenServer.cast(server, {:ticker_received, ticker, tickers})
  end


  # CALLBACKS

  def init(config) do
    # def_con = case config do
    #   %{} ->
    #     %SuperTrader.TradeManagerConfig{
    #         limit: 5,
    #         stop: 1,
    #         roc_threshold: 1,
    #         roc_period: 4
    #       }
    #   true ->
    #     config
    # end

    # BEST
    # def_con = %SuperTrader.TradeManagerConfig{
    #     limit: 100,
    #     stop: 1.5,
    #     roc_threshold: 1.3,
    #     roc_period: 5
    #   }
    #
    def_con = %SuperTrader.TradeManagerConfig{
        limit: 1.5,
        stop: 2,
        roc_threshold: 0.75,
        roc_period: 3
      }

    {:ok, %{
      config: def_con,
      trade_open: false,
      stop_order_filled: false,
      trades: [],
      orders: []
      }
    }
  end

  def handle_cast({:ticker_received, ticker, tickers}, state) do
    is_test = false
    stop_order_percent  = state.config.stop

    open_trade = Enum.find(state.trades, fn(x) -> x.currency_pair == ticker.id end)

    state = case open_trade do
      nil ->
        state = case open_trade(ticker, state) do
          :error ->
            state

          {:ok, state} ->
            state

          {:ok, state, order_id} ->
            state
        end

      trades when is_list trades ->
        case Enum.find(trades, fn(x) -> x.amount < 0.0001 end) do
          nil ->
            Enum.reduce(trades, state, fn(x, acc) ->
              acc = analyse_trade_state(x, ticker, tickers, stop_order_percent, is_test, acc)
            end)
          dilinquent_trade ->
            total_trade_amount = Enum.reduce(trades, fn(x, acc) -> acc + x.amount end)
            state
            |> remove_trade_from_state(dilinquent_trade)
            |> add_amount_to_next_trade(trades, dilinquent_trade)
        end

      trade ->
        analyse_trade_state(trade, ticker, tickers, stop_order_percent, is_test, state)
    end
    # IO.puts "State after receive ticker"
    # IO.inspect state
    {:noreply, state}
  end

  # defp check_for_open_trades(ticker, state) do
  #   period_end_time = DateTime.utc_now |> DateTime.to_unix
  #   period_start_time = period_end_time - 240
  #   case ExchangeApi.return_trade_history(ticker.id, period_start_time, period_end_time) do
  #     :error ->
  #
  #     {:ok, trades} ->
  #       trade = Enum.filter(trades, fn(x) ->
  #         x.currency_pair == ticker.id and x.type == "buy"
  #       end)
  #
  #   end
  # end

  defp remove_trade_from_state(state, trade) do
    trades = Enum.filter(state.trades, fn(x) -> x.trade_id != trade.trade_id end)
    Map.put(state, :trades, trades)
  end

  defp add_trade_to_state(state, trade) do
    trades = [trade| state.trades]
    Map.put(state, :trades, trades)
  end

  defp add_amount_to_next_trade(state, currency_trades, dilinquent_trade) do
    next_currency_trade = Enum.find(currency_trades, fn(x) ->
      x.trade_id == dilinquent_trade.trade_id
    end)

    updated_amount = dilinquent_trade.amount + next_currency_trade.amount
    updated_trade = Map.put(next_currency_trade, :amount, updated_amount)

    state
    |> remove_trade_from_state(next_currency_trade)
    |> add_trade_to_state(updated_trade)
  end

  defp analyse_trade_state(opening_trade, ticker, tickers, stop_order_percent, is_test, state) do
    # IO.puts "Trade Manager State"
    # IO.inspect state

    case StopLimit.execute(ticker, opening_trade, state.config.limit) do
      {:close, limit_value} ->
        case place_stop_order(ticker, limit_value, state) do
          {:ok, state} ->
            IO.puts "Exectued stop Limit"
            IO.inspect state
            IO.inspect limit_value
            Enum.reduce(state.trades, state, fn(stop_trade, new_state) ->
              new_state = do_place_stop_order(state, opening_trade, stop_trade, ticker, limit_value, is_test)
            end)

          {:error, state} ->
            IO.puts "Stop loss order failed for " <> ticker.id
            state
        end

      {:ok} ->
        previous_ticker = tickers |> Enum.take(2) |> List.last
        state = case SoftTrailingStop.execute(ticker, previous_ticker, opening_trade.stop_loss_value, stop_order_percent) do
          {:error, _} ->
            state

          {:close, stop_loss_value} ->
            case place_stop_order(ticker, stop_loss_value, state) do
              {:ok, state} ->
                IO.puts "Exectued stop Loss"
                IO.inspect state
                IO.inspect stop_loss_value
                Enum.reduce(state.trades, state, fn(stop_trade, new_state) ->
                  new_state = do_place_stop_order(state, opening_trade, stop_trade, ticker, stop_loss_value, is_test)
                end)

              {:error, state} ->
                IO.puts "Stop loss order failed for " <> ticker.id
                state
            end

          {:ok, stop_loss_value} ->
            #Stop loss changed
            opening_trade = Map.put(opening_trade, :stop_loss_value, stop_loss_value)
            state
            |> remove_trade_from_state(opening_trade)
            |> add_trade_to_state(opening_trade)
        end
    end
  end

  defp do_place_stop_order(state, opening_trade, stop_trade, ticker, stop_price, is_test) do

    # TODO this ios all wrong needs sorting

    # IO.puts "Stop loss no change - stop price, price, state"
    # IO.inspect stop_price
    # IO.inspect price
    # IO.inspect state

    case state.stop_order_filled do
      true ->
        IO.puts "Do place stop order trade, state - check trade is sell trade"
        IO.inspect stop_trade
        IO.inspect state
        case stop_price >= opening_trade.rate do
          true ->
            broadcast_profitable_trade(opening_trade, stop_trade, ticker, stop_price)
          false ->
            broadcast_loss_trade(opening_trade, stop_trade, ticker, stop_price)
        end

        # open_trades = Enum.filter(state.trades, fn(x) -> x.currency_pair != ticker.id end)
        # open_orders = Enum.filter(state.orders, fn(x) -> x.currency_pair != ticker.id end)
        state
          |> Map.put(:orders, [])
          |> Map.put(:trades, [])
          |> Map.put(:trade_open, false)
          |> Map.put(:stop_order_filled, false)

      false ->
        price = ticker.highest_bid
        case check_stop_order_has_been_filled(is_test, opening_trade, price, stop_price) do
          true ->
            case stop_price >= opening_trade.rate do
              true ->
                broadcast_profitable_trade(opening_trade, stop_trade, ticker, stop_price)
              false ->
                broadcast_loss_trade(opening_trade, stop_trade, ticker, stop_price)
            end

            # open_trades = Enum.filter(state.trades, fn(x) -> x.currency_pair != ticker.id end)
            # open_orders = Enum.filter(state.orders, fn(x) -> x.currency_pair != ticker.id end)
            state
              |> Map.put(:orders, [])
              |> Map.put(:trades, [])
              |> Map.put(:trade_open, false)
              |> Map.put(:stop_order_filled, false)
          false ->
            state
        end
    end
  end

  defp place_stop_order(ticker, rate, state) do
    amount = Enum.reduce(state.trades, 0, fn(x, acc) -> acc + x.amount end)
    amount = get_trade_amount_with_commission_applied(amount, rate)

    case ExchangeApi.place_stop_order(:exchange_api, :test, ticker.id, rate, amount) do
      {:ok, order} ->
        IO.puts "Place stop order: Order ->"
        IO.inspect order
        state = case order.resulting_trades do
          [] ->
            state
            |> Map.put(:stop_order_filled, false)
          trades ->
            state
            |> Map.put(:stop_order_filled, true) # TODO: put this against trade not state
            |> Map.put(:trades, trades)
        end
        {:ok, state}
      :error ->
        state
        |> Map.put(:stop_order_filled, false)
        {:error, state}
    end
  end

  defp broadcast_profitable_trade(opening_trade, closing_trade, ticker, stop_price) do
    traded_rate = opening_trade.rate
    amount = closing_trade.amount
    sell_rate = closing_trade.rate
    currency_pair = ticker.id
    profit = (sell_rate - traded_rate) * amount

    SuperTrader.Endpoint.broadcast "trading:lobby", "trade_closed:profit", %{
      "currency_pair" => currency_pair,
      "rate" => sell_rate,
      "amount" => amount,
      "profit" => profit,
      "profit_gbp" => profit * 2000
    }

    SuperTrader.Endpoint.broadcast "trading:lobby", "trade_closed", closing_trade

    # SuperTrader.TradingMonitor.trade_closed(:trading_monitor, trade)
  end

  defp broadcast_loss_trade(opening_trade, closing_trade, ticker, stop_price) do
    traded_rate = opening_trade.rate
    amount = closing_trade.amount
    sell_rate = stop_price
    currency_pair = ticker.id
    loss = (traded_rate - sell_rate) * amount

    SuperTrader.Endpoint.broadcast "trading:lobby", "trade_closed:loss", %{
      "currency_pair" => currency_pair,
      "rate" => sell_rate,
      "amount" => amount,
      "loss" => loss,

      "loss_gbp" => loss * 2000
    }

    SuperTrader.Endpoint.broadcast "trading:lobby", "trade_closed", closing_trade

    # SuperTrader.TradingMonitor.trade_closed(:trading_monitor, trade)
  end

  defp broadcast_trade_opened(trades, order) do
    Enum.each(trades, fn(trade) ->
      SuperTrader.Endpoint.broadcast "trading:lobby", "trade_opened", trade
      SuperTrader.TradingMonitor.trade_opened(:trading_monitor, trade)
    end)
  end

  defp check_stop_order_has_been_filled(is_test, trade, price, stop_price) do
    IO.puts "Checking stop order - Price, Stop Price"
    IO.inspect price
    IO.inspect stop_price

    case is_test do
      true ->
        price <= stop_price

      false ->
        case ExchangeApi.return_open_orders(:exchange_api, trade.currency_pair) do
          {:ok, orders} ->
            length(orders) == 0
          :error ->
            false
        end
    end
  end

  defp open_trade(ticker, state) do

    cond do
      can_open_trade(state) ->
        case SuperTrader.Strategies.PriceIncreasing.test_trigger(ticker, state.config.roc_period, state.config.roc_threshold) do
           true ->
             do_open_trade(state, ticker)
            #  case ExchangeApi.get_balance(:exchange_api, "BTC") do
            #    {:ok, balance} ->
            #      cond do
            #        balance >= @min_balance ->
            #           do_open_trade(state, ticker)
            #         true -> :error #TODO: Should be something other than error
            #       end
            #     :error -> :error
            #   end
          false ->
            :error
        end
      true ->
        :error
    end
  end

  defp can_open_trade(state) do
    state.trade_open == false
    && length(state.trades) < 1
  end

  defp do_open_trade(state, ticker) do
    IO.puts("Trade Triggered for Price Increasing")

    trade_value = TradeValueCalculator.get_trade_value(ticker.id)
    rate = ticker.lowest_ask
    amount = get_trade_amount(trade_value, rate)

    case place_trade(ticker.id, rate, amount, state) do
      {state, nil} ->
        IO.puts "Open Trade - This shouldn't happen"
        {:ok, state}

      {state, order_id} ->
        state = Map.put(state, :trade_open, true)
        {:ok, state, order_id}
    end
  end

  defp get_lowest_ask(currency_pair) do
    ExchangeApi.get_lowest_ask(:exchange_api, currency_pair)
  end

  defp get_highest_bid(currency_pair) do
    ExchangeApi.get_highest_bid(:exchange_api, currency_pair)
  end

  defp get_trade_amount(trade_value, rate) do
    trade_value / rate
  end

  def get_trade_amount_with_commission_applied(amount, rate) do
    cost = amount * rate
    commission = (cost * 0.0025)
    purchase_value = cost - commission
    actual_trade_amount = purchase_value / rate
  end

  defp place_trade(currency_pair, rate, amount, state) do
    case ExchangeApi.execute_trade(:exchange_api, :test, currency_pair, rate, amount, "buy") do
      :error ->
        {state, nil}
      {:ok, order} ->
        case order.resulting_trades do
          [] ->
            IO.puts "Place Trade - This shouldn't happen"
            case ExchangeApi.return_order_trades(:exchange_api, order.order_number) do
              {:ok, trades} ->
                trades = set_initial_stop_loss(state, trades)
                broadcast_trade_opened(trades, order)

                state = Map.put(state, :trades, trades)
                {state, order.order_number}
              :error ->
                {state, nil}
            end
          trades ->
            trades = set_initial_stop_loss(state, trades)
            broadcast_trade_opened(trades, order)

            state = Map.put(state, :trades, trades)
            {state, order.order_number}
        end
    end
  end

  defp set_initial_stop_loss(state, trades) when is_list trades do
    Enum.map(trades, fn(trade) ->
      set_initial_stop_loss(state, trade)
    end)
  end

  defp set_initial_stop_loss(state, trade) do
    stop_loss = trade.rate - (trade.rate * (state.config.stop/100))
    Map.put(trade, :stop_loss_value, stop_loss)
  end

  # def terminate(reason, state) do
  #   # Do Shutdown Stuff
  #
  #   IO.puts "Going Down: "
  #   :normal
  # end
end
