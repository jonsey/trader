defmodule SuperTrader.MarketTradeManager do
  use GenServer

  alias SuperTrader.{ExchangeApi, MarketTradeItem}

  def start_link(name) do
   GenServer.start_link(__MODULE__, :ok, name: name)
  end

  def receive_trade(server, market_trade_item) do
    GenServer.call(server, {:receive_trade, market_trade_item})
  end

  def get_volume(server, currency_pair, period) do
    GenServer.call(server, {:get_volume, currency_pair, period})
  end

  def get_average_volume_for_period(server, currency_pair, interval_in_seconds, period_in_seconds) do
    GenServer.call(server, {:get_average_volume_for_period, currency_pair, interval_in_seconds, period_in_seconds})
  end

  def init(:ok) do
    market_trade_items = %{}
    vol60 = %{}
    {:ok, %{market_trade_items: market_trade_items, vol60: vol60}}
  end

  def handle_call({:receive_trade, market_trade_item}, _from, state) do
    state = cleanup_market_trade_items_state(market_trade_item, state)

    vol60 = update_60_second_volume(market_trade_item, state)
    market_trade_items = update_market_trade_items(market_trade_item, state)
    state = Map.put(state, :vol60, vol60)
    state = Map.put(state, :market_trade_items, market_trade_items)

    {:reply, :ok, state}
  end

  defp cleanup_market_trade_items_state(market_trade_item, state) do
    currency_trade_items = case Map.get(state.market_trade_items, market_trade_item.currency_pair |> String.to_atom) do
      nil ->
        []
      trade_items ->
        cond do
          length(trade_items) == 1000 ->
            trade_items = pop_last_element_and_store(trade_items)
            [market_trade_item | trade_items]
          true ->
            [market_trade_item | trade_items]
        end
    end

    market_trade_items = Map.put(state.market_trade_items, market_trade_item.currency_pair |> String.to_atom, currency_trade_items)
    Map.put(state, :market_trade_items, market_trade_items)
  end

  def handle_call({:get_volume, currency_pair, period}, _from, state) do
    vol60 = case period do
      60 ->
        case Map.get(state.vol60, currency_pair |> String.to_atom) do
          nil ->
            nil # No trades registered yet
          [latest | _] ->
            latest
        end
      _ ->
        nil
    end
    {:reply, vol60, state}
  end

  defp currency_pair_key(%MarketTradeItem{currency_pair: currency_pair} = market_trade_item) do
    currency_pair |> String.to_atom
  end

  def handle_call({:get_average_volume_for_period, currency_pair, interval_in_seconds, period_in_seconds}, _from, state) do
    end_time = DateTime.utc_now
    {:ok, start_time} = (DateTime.to_unix(end_time) - period_in_seconds) |> DateTime.from_unix

    trade_items = case Map.get(state.market_trade_items, currency_pair |> String.to_atom) do
      nil ->
        # IO.puts "Map.get(state.market_trade_items, currency_pair |> String.to_atom) == nil"
        period_end_time = DateTime.utc_now |> DateTime.to_unix
        period_start_time = period_end_time - period_in_seconds
        trade_history = ExchangeApi.return_trade_history(:exchange_api, currency_pair, period_start_time, period_end_time)
        Map.put(state.market_trade_items, currency_pair |> String.to_atom, trade_history)
      items ->
        state.market_trade_items
    end
    #
    # IO.puts "Trade Items"
    # IO.inspect trade_items

    currency_trade_items = Map.get(trade_items, currency_pair |> String.to_atom)
    trade_items_in_interval = Enum.filter(currency_trade_items, fn(x) ->
      trade_item_is_in_interval(x, start_time, end_time)
    end)
    #
    # IO.puts "trade_items_in_interval"
    # IO.inspect trade_items_in_interval

    average = get_average_for_interval(trade_items_in_interval, period_in_seconds, interval_in_seconds)

    {:reply, average, state}
  end

  defp trade_item_is_in_interval(trade_item, start_time, end_time) do
    # IO.puts "start end trade item date"
    # IO.inspect start_time
    #   IO.inspect end_time
    #     IO.inspect trade_item.date
    (:gt == DateTime.compare(trade_item.date,  start_time)) and (:lt == DateTime.compare(trade_item.date, end_time))
  end

  defp get_average_for_interval(trade_items_in_interval, period_in_seconds, interval_in_seconds) do
    volume = Enum.reduce(trade_items_in_interval, 0.00, fn(x, acc) ->
      x.amount + acc
    end)

    ratio = period_in_seconds / interval_in_seconds
    volume / ratio
  end

  defp update_market_trade_items(market_trade_item, state) do
    currency_pair = market_trade_item.currency_pair

    cond do
      Map.get(state.market_trade_items, currency_pair |> String.to_atom) == nil ->
        end_time = DateTime.utc_now |> DateTime.to_unix
        start_time = end_time - (600)
        trade_history = ExchangeApi.return_trade_history(:exchange_api, currency_pair, start_time, end_time)
        # IO.puts "Trade History"
        # IO.inspect trade_history
        Map.put_new(state.market_trade_items, currency_pair |> String.to_atom, trade_history)
      true ->
        # IO.puts "Market Trade Item"
        # IO.inspect market_trade_item
        Map.put(state.market_trade_items, currency_pair |> String.to_atom, [market_trade_item | Map.get(state.market_trade_items, currency_pair |> String.to_atom)])
    end
  end

  defp update_60_second_volume(market_trade_item, state) do
    vol = get_trade_vol(market_trade_item)
    latest_vol60 = get_latest_vol60(market_trade_item.currency_pair, state)

    vol60 = case latest_vol60 do
      nil ->
        Map.put_new(state.vol60, market_trade_item.currency_pair |> String.to_atom , [vol] )
      vol60 ->
        cond do
          market_trade_item.date.minute > latest_vol60.date ->
            Map.put(state.vol60, market_trade_item.currency_pair |> String.to_atom , [vol] )
          true ->
            new_vol = Map.put(latest_vol60, :vol, latest_vol60.vol + vol.vol)
            Map.put(state.vol60, market_trade_item.currency_pair |> String.to_atom , [new_vol])


        end
    end
    # IO.puts "Vol60:"
    # IO.inspect vol60
    vol60
  end

  defp get_trade_vol(market_trade_item) do
    vol = market_trade_item.amount
    # IO.puts "Market trade item"
    # IO.inspect market_trade_item
    if(market_trade_item.type == "sell") do
      vol = vol * -1
    end
    %{date: market_trade_item.date.minute, vol: vol}
  end

  defp get_latest_vol60(currency_pair, state) do
    case Map.get(state.vol60, currency_pair |> String.to_atom) do
      nil ->
        nil
      [h | _] ->
        h
    end
  end

  defp pop_last_element_and_store(list) do
    list
      |> Enum.reverse
      |> send_to_storage
  end

  defp send_to_storage([last_item | rest]) do
    # Task.async store_ticker(last_item)
    Enum.reverse rest
  end

  defp store_ticker(market_trade_item) do
    {:ok}
  end

end
