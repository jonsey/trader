defmodule SuperTrader.TradeValueCalculator do

  alias SuperTrader.ExchangeApi

  @number_of_trades_avaliable_to_open 5

  def get_trade_value(currency_pair) do
    # 0.001 # £5
    # 0.0025 # £5
    # 0.00454348 # £10
    # 0.011363636 # £20
    # 0.017045455 # £30
    0.003
    #  0.025104091 # £50
    # 0.056818182 # £100
  end

  # defp get_trade_value() do
  #   get_current_balance
  #   |> calculate_single_trade_value
  # end
  #
  # defp get_current_balance() do
  #   case endExchangeApi.get_balance(:exchange_api, "BTC") do
  #     {:ok, balance} ->
  #       balance
  #     :error ->
  #       0
  #   end
  # end
  #
  # defp calculate_single_trade_value(current_balance) do
  #   current_balance / @number_of_trades_avaliable_to_open
  # end
end

# Current btc 0.02941519
