defmodule SuperTrader.TradeResponseAnalyser do

  @expected_fields ~w(
    orderNumber resultingTrades
  )

  def analyse_response(exchange_response) do
    case exchange_response do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        decoded_body =
          body
          |> Poison.decode!
          |> Map.take(@expected_fields)
          |> Enum.map(fn({k, v}) -> {String.to_atom(k), v} end)
          |> Kernel.put_in([:tradeStatus], {:error, :no_trade})

        {:ok, decoded_body}
      _ ->
        {:error, :request_failed}
    end
  end
end
