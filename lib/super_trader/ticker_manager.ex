defmodule SuperTrader.TickerManager do
  use GenServer

  alias SuperTrader.{Repo, PredictionTradeManager, TickerItem, ExchangeApi}

  # API

  def start_link(name) do
    GenServer.start_link(__MODULE__, :ok, name: name)
  end

  def get_all_tickers(server, raw_tickers) do
    GenServer.cast(server, {:get_all_tickers, raw_tickers})
  end

  def receive_ticker(server, ticker) do
    GenServer.cast(server, {:receive_ticker, ticker})
  end

  def get_current_state(server) do
    GenServer.call(server, {:get_current_state})
  end

  def get_currency_tickers(server, currency_pair, from_date) do
    GenServer.call(server, {:get_currency_tickers, currency_pair, from_date})
  end

  def get_latest_ticker(server, currency_pair) do
    GenServer.call(server, {:get_latest_ticker, currency_pair})
  end

  # CALLBACKS

  def init(:ok) do
    tickers = %{}
    # :timer.apply_interval(:timer.seconds(1), SuperTrader.TickerManager, :get_tickers, [])
    # get_tickers()
    {:ok, %{tickers: tickers}}
  end

  defp get_tickers() do
    case ExchangeApi.get_all_tickers(:exchange_api) do
      :error ->
        IO.puts "No tickers"
        nil

      {:ok, tickers} ->
        Enum.each(tickers, fn(ticker) ->
          if (ticker.id |> String.starts_with?("BTC-")) do
            SuperTrader.TickerManager.receive_ticker(:ticker_manager, ticker)
          end
        end)
    end
  end

  def handle_call({:get_current_state}, _from, state) do
    {:reply, state, state}
  end

  def handle_call({:get_latest_ticker, currency_pair}, _from, state) do
    period_in_seconds = 300
    {:ok, from_date} = DateTime.to_unix(DateTime.utc_now) - period_in_seconds |> DateTime.from_unix
    currency_tickers = Repo.load_tickers(currency_pair, from_date)
    IO.puts("Currency Tickers -------------------------------------------------")
    IO.inspect currency_tickers
    IO.inspect currency_pair
    ticker = Enum.find(currency_tickers, fn(x) -> x.id == currency_pair end)
    {:reply, ticker, state}
  end

  def handle_call({:get_currency_tickers, currency_pair, from_date}, _from, state) do
    tickers = Repo.load_tickers(currency_pair, from_date)
    {:reply, tickers, state}
  end

  def handle_cast({:get_all_tickers, raw_tickers}, state) do
    case raw_tickers do
      {:ok, tickers} ->
        Enum.each(tickers, fn({k, v}) ->
          ticker = TickerItem.map_ticker(k, v)
          GenServer.cast(:ticker_manager, {:receive_ticker, ticker} )
        end)

      {:error, reason} ->
        IO.puts("Error fetching all tickers")
    end
    {:noreply, state}
  end

  def handle_cast({:receive_ticker, ticker}, state) do
    try do
      PredictionTradeManager.ticker_received(ticker.id |> String.to_atom, ticker)
    catch
      x ->  IO.inspect x
    end

    {:noreply, state}
  end


  defp pop_last_element_and_store(list) do
    list
      |> Enum.reverse
      |> send_ticker_to_storage
  end

  defp send_ticker_to_storage([last_item | rest]) do
    # Task.async store_ticker(last_item)
    Enum.reverse rest
  end

  defp store_ticker(ticker) do
    {:ok}
  end
end
