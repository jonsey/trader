defmodule SuperTrader.BittrexTradeManager do
  use GenServer

  alias SuperTrader.{BitrexExchangeApi, TradeValueCalculator, OrderItem}
  alias SuperTrader.ClosingStrategies.SoftTrailingStop

  @stop_loss_margin 1

  def start_link(name) do
    GenServer.start_link(__MODULE__, :ok, name: name)
  end

  def ticker_received(server, ticker, tickers) do
    GenServer.cast(server, {:ticker_received, ticker, tickers})
  end

  def init(:ok) do
    {:ok, %{
      trades: [],
      orders: [],
      stop_order_placed: false,
      buy_order_placed: false,
      buy_order_filled: false
    }}
  end

  def handle_cast({:ticker_received, ticker, tickers}, state) do
    currency_pair = ticker.id

    state = case open_trades_exist? state do
      true ->
        case stop_order_has_been_placed? state do
          true ->
            case stop_order_has_been_filled? currency_pair do
              true ->
                IO.puts "Stop order filled"
                IO.inspect state
                state = Map.put(state, :stop_order_placed, false)
                state = Map.put(state, :buy_order_placed, false)
                open_new_trade_if_triggered ticker, state

              false -> state
            end

          false ->
            close_trades_if_below_margin state.trades, state, ticker, tickers
        end

      false ->
        # IO.puts "No open trades"
        # IO.inspect state
        open_new_trade_if_triggered ticker, state
    end
    {:noreply, state}
  end

  defp open_trades_exist?(state) do
    IO.inspect state
    state.buy_order_placed
  end

  defp stop_order_has_been_placed?(state) do
    state.stop_order_placed
  end

  defp stop_order_has_been_filled?(currency_pair) do
    case BitrexExchangeApi.return_open_orders(:bitrex_exchange_api, currency_pair) do
      {:ok, orders} ->
        length(orders) == 0
      :error
        false
    end
  end

  defp open_new_trade_if_triggered(ticker, state) do
    # Needs more thought, could still be too much risk
    stop_loss_value = ticker.last - (ticker.last * (@stop_loss_margin / 100))
    highest_bid_threshold = ticker.last - (ticker.highest_bid * (@stop_loss_margin / 50))

    IO.puts "Stop Loss Value, Highest Bid Threshold"
    IO.inspect stop_loss_value
    IO.inspect highest_bid_threshold

    case stop_loss_value <= highest_bid_threshold do
      true ->
        IO.puts "Highest bid is too low"
        state
      false ->
        case SuperTrader.Strategies.PriceIncreasing.test_trigger(ticker) do
           true ->
              IO.puts("Trade Triggered for Price Increasing")
              case BitrexExchangeApi.get_balance(:bitrex_exchange_api, "BTC") do
                {:ok, balance} ->
                  cond do
                    balance > 0.0235 ->
                      trade_value = TradeValueCalculator.get_trade_value(ticker.id)
                      rate = ticker.lowest_ask
                      amount = trade_value / rate
                      case place_trade(ticker.id, rate, amount) do
                        nil ->
                          state
                        {order, trade_amount} ->
                          orders = [order | state.orders]
                          state = Map.put(state, :orders, orders)
                          trades = order.resulting_trades
                          state = Map.put(state, :trades, trades)

                          IO.inspect amount
                          IO.inspect trade_amount

                          state = Map.put(state, :buy_order_placed, true)

                          case (amount - trade_amount) < 0.0000001 do
                            true ->
                              Map.put(state, :buy_order_filled, true)

                            false ->
                              Map.put(state, :buy_order_filled, false)
                          end
                      end

                    true ->
                      IO.puts "Not enough avialable BTC"
                      IO.inspect balance
                      state
                  end
                :error ->
                  IO.puts "Error get BTC balance"
                  state
                end
          false ->
            state
        end
    end
  end

  defp place_trade(currency_pair, rate, amount) do
    case BitrexExchangeApi.execute_trade(:bitrex_exchange_api, :test, currency_pair, rate, amount, "buy") do
      :error ->
        nil
      {:ok, order_id} ->
        case BitrexExchangeApi.get_order(:bitrex_exchange_api, order_id) do
          {:ok, order} ->
            trades = order.resulting_trades
            trade_amount = Enum.reduce(trades, 0.0, fn(x, acc) ->
              rate = x.rate
              amount = x.amount

              SuperTrader.Endpoint.broadcast "trading:lobby", "trade_opened", %{
                "currency_pair" => order.currency_pair,
                "order_number" => order.order_number,
                "trades" => trades,
                "rate" => rate,
                "amount" => get_trade_amount_with_commission_applied(amount, rate), # This will be already calced for live trades
                "date" => DateTime.utc_now
              }

              acc = acc + amount
            end)
            {order, trade_amount}
          :error ->
            nil
        end
    end
  end

  def get_trade_amount_with_commission_applied(amount, rate) do
    cost = amount * rate
    commission = (cost * 0.0025)
    purchase_value = cost - commission
    actual_trade_amount = purchase_value / rate
  end

  defp close_trades_if_below_margin(trades, state, current_ticker, tickers) do
    updated_trades = []

    case state.buy_order_filled do
      true ->
        previous_ticker = tickers |> Enum.take(2) |> List.last

        IO.puts "close_trades_if_below_margin - Trades"
        IO.inspect trades
        Enum.reduce(trades, state, fn(trade, acc) ->

          trade = case trade.stop_loss_value do
            nil ->
              stop_loss_value = trade.rate - (trade.rate * (@stop_loss_margin / 100))
              Map.put(trade, :stop_loss_value, stop_loss_value)
            value -> trade
          end

          stop_loss_value = trade.stop_loss_value

          IO.puts "Initial stop loss value"
          IO.inspect stop_loss_value


          case SoftTrailingStop.execute(current_ticker, previous_ticker, stop_loss_value, @stop_loss_margin) do
            {:ok, value} ->
              IO.puts "Stop value changed"
              IO.inspect value
              trade = Map.put(trade, :stop_loss_value, value)
              updated_trades = [trade | updated_trades]
              acc = Map.put(acc, :trades, updated_trades)
            {:close, value} ->
              # Lookat the stop rate needs more thought
              case place_stop_order(trade, current_ticker.highest_bid) do
                {:ok, stop_order} ->
                  orders = [stop_order | acc.orders]
                  updated_trades = Enum.filter(acc.trades, fn(x) -> x.trade_id != trade.trade_id end)
                  acc = Map.put(acc, :trades, updated_trades)
                  acc = Map.put(acc, :orders, orders)
                  acc = Map.put(acc, :stop_order_placed, true)
                  acc = Map.put(acc, :buy_order_placed, false)
                  acc = Map.put(acc, :buy_order_filled, false)
                :error ->
                  IO.puts "Error placing stop loss order for"
                  IO.inspect trade
                  acc
              end
          end
        end)
    false ->
      case BitrexExchangeApi.return_open_orders(:bitrex_exchange_api, current_ticker.id) do
        {:ok, []} ->
           Map.put(state, :buy_order_filled, true)
        {:ok, orders} ->
          IO.puts "Open Orders response"
          IO.inspect orders
          IO.puts "State"
          IO.inspect state # Update trades and set buy_order_filled

          IO.puts "Has open order - fix this bit of code"

          resulting_trades = Enum.each(orders, fn(x) -> x.resulting_trades end)

          state = Map.put(state, :orders, orders)
          Map.put(state, :trades, resulting_trades)

        :error ->
          IO.puts "Error fetching open orders"
          state
      end
    end
  end

  defp place_stop_order(trade, rate) do
    currency_pair = trade.currency_pair
    amount = trade.amount
    BitrexExchangeApi.place_stop_order(:bitrex_exchange_api, :test, currency_pair, rate, amount)
  end

end
