defmodule SuperTrader.OrderbookManager do
  use GenServer

  alias SuperTrader.{OrderbookItem}

  def start_link(name) do
   GenServer.start_link(__MODULE__, :ok, name: name)
  end

  def receive_update(server, orderbook_item) do
    # IO.inspect orderbook_item
    GenServer.call(server, {:receive_update, orderbook_item})
  end

  # CALLBACKS

  def init(:ok) do
    orderbook_items = %{}
    {:ok, {orderbook_items}}
  end

  def handle_call({:receive_update, orderbook_item}, _from, {orderbook_items} = state) do

    orderbook_items = case orderbook_items[orderbook_item.seq] do
      nil ->
        IO.puts "none exists"
        add_orderbook_item orderbook_item, state
      item ->
        %{}
        # update_orderbook_item_value orderbook_items, item
    end
    {:reply, :ok, {orderbook_items}}
  end

  defp add_orderbook_item(orderbook_item, {orderbook_items} = state) do
    Map.put(orderbook_items, orderbook_item.seq, orderbook_item)
  end

end
