defmodule SuperTrader.Strategies.AverageMarketVolume do

  alias SuperTrader.{MarketTradeManager, ExchangeApi}

  def test_trigger(ticker) do

    do_trigger = case ticker.is_frozen do
      false ->
        interval_in_seconds = 60
        period_in_seconds = 600
        average_trade_volume = MarketTradeManager.get_average_volume_for_period(:market_trade_manager, ticker.id, interval_in_seconds, period_in_seconds)
        volume = MarketTradeManager.get_average_volume_for_period(:market_trade_manager, ticker.id, 60, 60)
        base_volume = ticker.base_volume
        percentage_change =  ticker.percent_change

        IO.puts "Average Vol , Trade Volume, base vol, percent change" <> ticker.id
        IO.inspect average_trade_volume
        IO.inspect volume
        IO.inspect base_volume
        IO.inspect percentage_change

        # SuperTrader.Endpoint.broadcast "trading:lobby", "ticker_monitor", %{
        #   "currency_pair" => ticker.id,
        #   "average_trade_volume" => average_trade_volume,
        #   "volume" => volume,
        #   "percentage_change" => percentage_change,
        #   "base_volume" => base_volume
        # }

        cond do
          base_volume > 1000 and percentage_change > 0.35 and volume > 0 and average_trade_volume > 0 ->
            volume > average_trade_volume
          true ->
            false
        end

      true ->
        IO.puts "Frozen"
        false
    end
  end

  defp trigger_trade?(ticker, data) do
    false
  end

end
