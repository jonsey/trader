defmodule SuperTrader.Strategies.PercentageIncrease do

  alias SuperTrader.{MarketTradeManager, ExchangeApi}

  def test_trigger(ticker) do

    do_trigger = case ticker.is_frozen do
      false ->
        base_volume = ticker.base_volume
        percentage_change =  ticker.percent_change

        # IO.puts "Average Vol , Trade Volume, base vol, percent change" <> ticker.id
        # IO.inspect base_volume
        # IO.inspect percentage_change

        cond do
          base_volume > 1000 and percentage_change > 0.25  ->
            true
          true ->
            false
        end

      true ->
        IO.puts "Frozen"
        false
    end
  end

  defp trigger_trade?(ticker, data) do
    false
  end

end
