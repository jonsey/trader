defmodule SuperTrader.Strategies.MarketVolume do

  alias SuperTrader.{MarketTradeManager}

  def test_trigger(ticker) do

    case ticker.is_frozen do
      false ->
        period = 300
        start_time = start_time(300)
        end_time = 9999999999

        trigger_trade?(ticker, period, start_time, end_time)
      true ->
        IO.puts "Frozen"
        false
    end
  end

  defp trigger_trade?(ticker, period, start_time, end_time) do

    volume = volume_for_period(ticker.id, period, start_time, end_time)
    # IO.puts "Volume"
    # IO.inspect volume
    # volume > 100
    quote_volume = ticker.quote_volume
    trigger_level = (volume / quote_volume)
    if(trigger_level > 0) do
      # IO.puts "Checking volume - vol / quote vol / trigger" <> ticker.id
      # IO.inspect volume
      # IO.inspect quote_volume
      # IO.inspect trigger_level
    end
    trigger_level > 0.01
  end

  defp average_volume(day_vol, period) do
    day_vol / period
  end

  defp volume_for_period(currency_pair, period, start_time, end_time) do
    case MarketTradeManager.get_volume(:market_trade_manager, currency_pair, 60) do
      nil ->
        0
      vol ->
        vol.vol
    end
  end

  defp calculate_volume([h | []], acc) do
    h["volume"] + acc
  end

  defp calculate_volume([h | t], acc) do
    calculate_volume(t, h["volume"] + acc)
  end

  defp start_time(seconds_ago) do
    DateTime.utc_now()
    |> DateTime.to_unix()
    |> -seconds_ago
  end
end
