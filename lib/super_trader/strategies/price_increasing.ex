defmodule SuperTrader.Strategies.PriceIncreasing do

  alias SuperTrader.{TickerManager}

  def test_trigger(ticker, period, roc_threshold) do
    period_in_seconds = (period - 1) * 60
    {:ok, start_time} = DateTime.to_unix(DateTime.utc_now) - period_in_seconds |> DateTime.from_unix
    tickers = TickerManager.get_currency_tickers(:ticker_manager, ticker.id, start_time)

    tickers_in_period = Enum.filter(tickers, fn(x) ->
      DateTime.compare(x.date, start_time) == :gt
    end)

    IO.puts("Length of tickers in period")
    IO.inspect length(tickers_in_period)
    case (length(tickers_in_period) > period) do
      true ->
        [oldest_ticker|_] = tickers_in_period

        # tickers = Enum.take(tickers, ticker_to_check)
        base_volume = ticker.base_volume
        day_high = ticker.day_high

        cond do
          base_volume > 10 ->
            grouped_tickers = group_tickers(tickers_in_period)
            IO.puts("Length of grouped tickers")
            IO.inspect length(Map.keys(grouped_tickers))
            cond do
              length(Map.keys(grouped_tickers)) >= period ->
                only_increasing  = true
                spread_percentage = calculate_spread_percentage(ticker)
                # IO.puts "#{ticker.id} Only increasing"
                # IO.inspect only_increasing
                # IO.inspect grouped_tickers
                roc(grouped_tickers, ticker, period, roc_threshold)
                  && only_increasing(grouped_tickers)
                  && spread_percentage < 0.3
              true ->
                false
            end
            # current_price_is_greater_than_average(tickers, ticker)
            # is_increasing(tickers, true)
          true ->
            false
        end

      false ->
        false
    end

  end

  defp calculate_spread_percentage(ticker) do
    spread = ticker.lowest_ask - ticker.highest_bid
    percentage = (spread / ticker.last) * 100
    IO.puts("Spread")
    IO.inspect percentage
  end

  defp group_tickers(tickers) do
    grouped = Enum.reduce(tickers, %{}, fn(x, acc) ->
      case Map.get(acc, x.date.minute) do
        nil ->
          acc = Map.put(acc, x.date.minute, x.last)
        price ->
          case x.last > price do
            true -> Map.put(acc, x.date.minute, x.last)
            false -> acc
          end
      end
    end)
    grouped
  end

  defp roc(tickers, ticker, period, roc_threshold) do
    key_for_oldest_interval = DateTime.utc_now().minute - period + 1

    case Map.get(tickers, key_for_oldest_interval) do
      nil ->
        false
      last ->
        change = ticker.last - last

        cond do
          change > 0 ->
            roc = ((ticker.last - last) / (last)) * 100
            if roc > 0 do
              IO.puts ticker.id
              IO.inspect roc
            end
            roc >= roc_threshold
          true ->
            false
        end
    end
  end



  defp only_increasing(grouped_tickers) when is_map grouped_tickers do
    only_increasing(Map.values(grouped_tickers))
  end

  defp only_increasing([_h|[]]) do
    true
  end

  defp only_increasing(grouped_tickers) when is_list grouped_tickers  do
    [h|t] = grouped_tickers
    [next|rest] = t

    case h < next do
      true -> only_increasing(t)
      false -> false
    end
  end

  defp current_price_is_greater_than_average(tickers, ticker) do
    current_price = ticker.last
    average = average_price(tickers)
    average > current_price
  end

  defp average_price(tickers) do
    total_price = Enum.reduce(tickers, 0.00, fn(x, acc) ->
      acc + x.last
    end)
    total_price / length(tickers)
  end

  defp is_increasing(_, acc) when acc == false do
    false
  end

  defp is_increasing([h|[h1|[]]], acc) when acc do
    h.last > h1.last
  end

  defp is_increasing([h|[h1|t]], acc)  do
    is_increasing([h1|t], h.last > h1.last)
  end
end
