defmodule SuperTrader.Scalper do
  use GenServer

  def start_link(name, config) do
    GenServer.start_link(__MODULE__, config, [name: name])
  end

  def init(:ok) do
    {:ok, %{}}
  end

  def ticker_received(server, ticker, tickers) do
    GenServer.cast(server, {:ticker_received, ticker, tickers})
  end

  def handle_cast({:ticker_received, ticker, tickers}, state) do
    price_increasing = SuperTrader.Strategies.PriceIncreasing.test_trigger(ticker, 1, 0.2)

    state = case price_increasing do
      true ->
        maybe_open_trade(state, ticker)
      false ->
        state
    end

    {:noreply, state}
  end

  defp maybe_open_trade(state, ticker) do
    state = case open_trade_exists(state) do
      true -> state
      false -> open_trade(state, ticker)
    end
  end

  defp open_trade_exists(state) do
    length state.trades > 0
  end

  defp open_trade(state, ticker) do
    
  end
end
