defmodule SuperTrader.ClosingStrategies.SoftTrailingStop do

  def execute(current_ticker, previous_ticker, stop_loss_value, stop_loss_margin) do
    current_price = current_ticker.highest_bid
    last_price = previous_ticker.highest_bid
    percentage_change_in_price = change_in_price(current_price, last_price)

    # IO.puts "Current price, last price"
    # IO.inspect current_price
    # IO.inspect last_price

    case price_has_gone_up(percentage_change_in_price) do
      true ->
        stop_loss_value = increase_stop_loss_value(current_price, stop_loss_value, stop_loss_margin)
        {:ok, stop_loss_value}

      false ->
        case should_trigger_stop_loss(current_ticker, stop_loss_value) do
          true ->
            {:close, current_ticker.highest_bid}
          false ->
            {:ok, stop_loss_value}
        end
    end
  end

  defp change_in_price(current_price, last_price) do
    ((current_price - last_price) / last_price) * 100
  end

  defp price_has_gone_up(percentage_change_in_price) do
    percentage_change_in_price > 0
  end

  defp should_trigger_stop_loss(ticker, stop_loss_value) do
    ticker.highest_bid <= stop_loss_value
  end

  defp increase_stop_loss_value(current_price, stop_loss_value, stop_loss_margin) do
    stop_loss = current_price * ((100 - stop_loss_margin) / 100)

    case stop_loss < stop_loss_value do
      true -> stop_loss_value
      false -> stop_loss_value # set to hard limit
    end
  end





end
