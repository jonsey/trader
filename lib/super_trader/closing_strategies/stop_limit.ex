defmodule SuperTrader.ClosingStrategies.StopLimit do

  def execute(ticker, trade, limit_margin) do
    stop_limit = (1 + (limit_margin/100)) * trade.rate
    # IO.puts "Stop Limit: #{stop_limit |> Float.to_string}"
    cond do
      ticker.highest_bid >= stop_limit ->
        {:close, ticker.highest_bid}
      true -> {:ok}
    end
  end
end
