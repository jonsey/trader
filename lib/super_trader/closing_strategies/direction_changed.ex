defmodule SuperTrader.ClosingStrategies.DirectionChanged do
  alias SuperTrader.{TickerManager}

  def execute(ticker, opening_trade) do

    candled_tickers =
      TickerManager.get_currency_tickers(:ticker_manager, ticker.id)
      |> group_tickers()
      |> Map.values

    case length(candled_tickers) > 1 do
      true ->
        [current|[previous|[]]] = candled_tickers |> Enum.take 2
        margin_level = ticker.last * 0.998 # percentage lower than the last of the previous ticker
        case margin_level <= previous do
          true ->
            case ticker.highest_bid > (opening_trade.rate * 1.005) do
              true -> {:close, ticker.highest_bid}
              false -> {:ok}
            end

          false -> {:ok}
        end

      false -> {:ok}
    end
  end

  defp group_tickers(tickers) do
    grouped = Enum.reduce(tickers, %{}, fn(x, acc) ->
      case Map.get(acc, x.date.minute) do
        nil ->
          acc = Map.put(acc, x.date.minute, x.last)
        price ->
          case x.last > price do
            true -> Map.put(acc, x.date.minute, x.last)
            false -> acc
          end
      end
    end)
    grouped
  end

end
