defmodule SuperTrader.ClosingStrategies.TrailingStop do

  alias SuperTrader.{ExchangeApi}

  def execute(is_test, ticker, [h|t] = tickers, stop_order, stop_loss_precentage) do
    [previous_ticker| _] = t

    case ticker.last > previous_ticker.last do
        true ->
          move_trailing_stop(is_test, stop_order, ticker, stop_loss_precentage)
        false ->
          {:ok, :no_change}
    end
  end

  defp move_trailing_stop(is_test, stop_order, ticker, stop_loss_precentage) do
    price = ticker.highest_bid
    stop_price = price - (price * stop_loss_precentage)

    case is_test do
      true ->
        {:ok, stop_order}
      false ->
        case ExchangeApi.move_order(:exchange_api, stop_order.order_number, stop_price, ticker.id) do
          {:ok, order} ->
            {:ok, order}
          _ ->
            :error
        end
    end
  end

end
