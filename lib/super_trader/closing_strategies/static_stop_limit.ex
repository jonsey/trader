defmodule SuperTrader.ClosingStrategies.StaticStopLimit do

  alias SuperTrader.{ExchangeApi}

  def  execute(is_test, ticker, [h|t] = tickers, trade, stop_loss_precentage) do
    traded_rate = List.first(trade.resultingTrades).rate

    cond do
      ticker.highest_bid > (traded_rate * 1.005) ->
        amount = get_trade_amount_with_commission_applied(List.first(trade.resultingTrades).amount, traded_rate)
        sell_rate = ticker.highest_bid
        currency_pair = ticker.id

        case ExchangeApi.execute_trade(:exchange_api, :test, currency_pair, sell_rate, amount, "sell") do
          nil ->
            {:ok, :no_change}
          _ ->
          profit = (ticker.highest_bid - traded_rate) * amount

          SuperTrader.Endpoint.broadcast "trading:lobby", "trade_closed:profit", %{
            "currency_pair" => trade.currency_pair,
            "rate" => sell_rate,
            "amount" => amount,
            "profit" => profit,
            "profit_gbp" => profit * 1768
          }
          {:ok, sell_rate}
        end


      ticker.highest_bid < (traded_rate - (traded_rate *  0.005)) ->
        amount = get_trade_amount_with_commission_applied(List.first(trade.resultingTrades).amount, traded_rate)
        sell_rate = ticker.highest_bid
        currency_pair = ticker.id

        case ExchangeApi.execute_trade(:exchange_api, :test, currency_pair, sell_rate, amount, "sell") do
          nil ->
            {:ok, :no_change}
          _ ->
            loss = (traded_rate - sell_rate) * amount
            SuperTrader.Endpoint.broadcast "trading:lobby", "trade_closed:loss", %{
              "currency_pair" => trade.currency_pair,
              "rate" => sell_rate,
              "amount" => amount,
              "loss" => loss,

              "loss_gbp" => loss * 1768
            }
            {:ok, sell_rate}
        end

      true ->
        {:ok, :no_change}
    end
  end

  defp get_trade_amount_with_commission_applied(amount, rate) do
    cost = amount * rate
    commission = (cost * 0.0025)
    purchase_value = cost - commission
    actual_trade_amount = purchase_value / rate
  end

end
