defmodule SuperTrader.CapitalManager do
  use GenServer

  alias SuperTrader.ExchangeApi

  @number_of_trades_avaliable_to_open 10

  def start_link(name) do
    GenServer.start_link(__MODULE__, :ok, [name: name])
  end

  def init(:ok) do
    {:ok, %{
        current_balance: nil,
        current_trade_value: nil,
        time_of_last_balance: nil,
      }
    }
  end

  def get_state(server) do
    GenServer.call(server, {:get_state})
  end

  def get_trade_value(server) do
    GenServer.call(server, {:get_trade_value})
  end

  def current_balance(server) do
    GenServer.call(server, {:get_current_balance})
  end

  def handle_call({:get_state}, _from, state) do
    {:reply, state, state}
  end

  def handle_call({:get_trade_value}, _from, state) do
    state = do_get_trade_value(state)
    {:reply, state.current_trade_value, state}
  end

  def handle_call({:get_current_balance}, _from, state) do
    state = get_current_balance(state)
    {:reply, state.current_balance, state}
  end

  defp do_get_trade_value(state) do
    state
    |> get_current_balance
    |> calculate_single_trade_value
  end

  defp get_current_balance(state) do
    should_check_exchange = should_get_new_balance(state.time_of_last_balance)
    do_get_current_balance(state, should_check_exchange)
  end

  defp do_get_current_balance(state, should_check_exchange) when should_check_exchange == false do
    state
  end

  defp do_get_current_balance(state, should_check_exchange) when should_check_exchange do
    case ExchangeApi.get_complete_balances(:exchange_api) do
      {:ok, balances} ->
        balance = sum_balances(balances)
        state
        |> Map.put(:current_balance, balance)
        |> Map.put(:time_of_last_balance, DateTime.utc_now)
      :error ->
        state
    end
  end

  defp sum_balances(balances) do
    Enum.reduce  balances, 0.0,  fn {k, v}, acc ->
      btc_value = v["btcValue"] |> String.to_float
      acc = acc + btc_value
    end
  end

  defp should_get_new_balance(time_of_last_balance) when time_of_last_balance == nil do
    true
  end
  defp should_get_new_balance(time_of_last_balance) do
    period_in_seconds = 60 * 60
    {:ok, start_time} = DateTime.to_unix(DateTime.utc_now) - period_in_seconds |> DateTime.from_unix
    DateTime.compare(time_of_last_balance, start_time) == :lt
  end

  defp calculate_single_trade_value(state) do
    current_balance = state.current_balance
    trade_value = current_balance / @number_of_trades_avaliable_to_open
    Map.put(state, :current_trade_value, trade_value)
  end
end
