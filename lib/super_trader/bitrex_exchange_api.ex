defmodule SuperTrader.BitrexExchangeApi do
  use GenServer

  alias SuperTrader.{BitrexExchangeApi, TickerManager, TradeManager, PoisonApi, TickerItem, MarketTradeItem, OrderItem, DateTimeUtils}

  @api_url "https://bittrex.com/api/v1.1"
  @api_key "c73132e3dd744ea885337ce4686df73f"
  @secret "c5b5d537ab944071bc31ad7cb2e0f3d3"


  def start_link(name) do
    GenServer.start_link(__MODULE__, :ok, name: name)
  end

  def get_all_tickers(server) do
    GenServer.call(server, {:get_all_tickers})
  end

  def return_trade_history(server, currency_pair, start_time, end_time) do
    GenServer.call(server, {:return_trade_history, currency_pair, start_time, end_time})
  end

  def return_open_orders(server, currency_pair) do
    GenServer.call(server, {:return_open_orders, currency_pair})
  end

  def get_order(server, order_id) do
    GenServer.call(server, {:get_order, order_id})
  end

  def place_stop_order(server, is_test, currency_pair, rate, amount) do
    GenServer.call(server, {:place_stop_order, currency_pair, rate, amount})
  end

  def move_order(server, ordernumber, rate, currency_pair) do
    GenServer.call(server, {:move_order, ordernumber, rate, currency_pair})
  end

  def cancel_order(server, ordernumber) do
    GenServer.call(server, {:cancel_order, ordernumber})
  end

  def execute_trade(server, test_or_live, currency_pair, rate, amount, type) do
    GenServer.call(server, {:execute_live_trade, currency_pair, rate, amount, type})
  end

  def get_balance(server, currency) do
    GenServer.call(server, {:get_balance, currency})
  end


  # CALLBACKS

  def handle_call({:get_all_tickers}, _from, state) do
    # log_request "get_all_tickers"
    response = PoisonApi.get(@api_url <> "/public/getmarketsummaries")

    case response do
      {:ok, body} ->
        case body["success"] do
          true ->
            tickers = Enum.map(body["result"], fn(x) ->
              TickerItem.map_ticker(:bitrex, x)
            end)
            {:reply, {:ok, tickers}, state}
          false ->
            {:reply, :error, state}
        end

      {:error, _} ->
        {:reply, :error, state}
    end
  end

  def handle_call({:return_trade_history, currency_pair, start_time, end_time}, _from, state) do
    log_request "return_trade_history" <> currency_pair
    response = PoisonApi.get(@api_url <> "/public/getmarkethistory?market=#{currency_pair}")
    # IO.inspect trade_data_response

    trade_data = case response do
      {:ok, data} ->
        Enum.map(data, fn(x) -> MarketTradeItem.map_trade_data(:bitrex, x, currency_pair) end)
      _ ->
        nil
    end

    {:reply, trade_data, state}
  end

  def handle_call({:return_open_orders, currency_pair}, _from, state) do
    log_request "return_open_orders"

    nonce = System.system_time(:microsecond) |> Integer.to_string
    url = @api_url <> "/market/getopenorders?apikey=#{@api_key}&nonce=#{nonce}&market=#{currency_pair}"
    headers = [ApiSign: sign(@secret, url), "Content-Type": "application/x-www-form-urlencoded"]

    # IO.inspect body
    orders = case PoisonApi.get(url, headers, []) do
      {:ok, body } ->
        case body["success"] do
          true ->
            IO.puts "Open orders response"
            IO.inspect body
            case body["result"] == [] do
              true -> {:ok, []}
              false ->
                orders = Enum.map(body["result"], fn(x) ->
                  OrderItem.map_data_bitrex(x)
                end)
                {:ok, orders}
            end

          false -> :error
        end
      {:error, _} ->
        :error
      end

      {:reply, orders, state}
  end

  def handle_call({:get_order, order_id}, _from, state) do
    log_request "get_order"

    nonce = System.system_time(:microsecond) |> Integer.to_string
    url = @api_url <> "/account/getorder?apikey=#{@api_key}&nonce=#{nonce}&uuid=#{order_id}"
    headers = [ApiSign: sign(@secret, url), "Content-Type": "application/x-www-form-urlencoded"]

    # IO.inspect body
    order = case PoisonApi.get(url, headers, []) do
      {:ok, body } ->
        IO.inspect body
        case body["success"] do
          true -> {:ok, OrderItem.map_data_bitrex(body["result"])}
          false -> :error
        end
      {:error, _} ->
        :error
      end

      {:reply, order, state}
  end

  def handle_call({:place_stop_order, currency_pair, rate, amount}, _from, state) do
    log_request "Place stop order"

    nonce = System.system_time(:microsecond) |> Integer.to_string
    amount = amount |> Float.to_string
    rate = rate |> Float.to_string

    url = @api_url <> "/market/selllimit?apikey=#{@api_key}&nonce=#{nonce}&market=#{currency_pair}&quantity=#{amount}&rate=#{rate}"
    headers = [ApiSign: sign(@secret, url), "Content-Type": "application/x-www-form-urlencoded"]

    # IO.inspect body
    order = case PoisonApi.get(url, headers, []) do
      {:ok, body }->
        IO.inspect body
        case body["success"] do
          true ->
            result = body["result"]
            order_id = result["uuid"]
            {:ok, order_id}

          false -> :error
        end
      {:error, _} ->
        :error
    end
    {:reply, order, state}
  end

  def handle_call({:move_order, ordernumber, rate, currency_pair}, _from, state) do
    log_request "move_order"

    order = case BitrexExchangeApi.get_order(:bitrex_exchange_api, ordernumber) do
      {:ok, order} ->
        trades = order.resulting_trades
        amount = Enum.reduce(trades, fn(x, acc) ->
          acc = x.amount + acc
        end)

        order = case BitrexExchangeApi.cancel_order(:bitrex_exchange_api, ordernumber) do
          :ok ->
            amount = amount |> Float.to_string
            rate = rate |> Float.to_string

            BitrexExchangeApi.place_stop_order(:bitrex_exchange_api, :test, currency_pair, rate, amount)
          :error ->
            :error
        end
      :error ->
        :error
    end
    {:reply, order, state}
  end

  def handle_call({:cancel_order, ordernumber}, _from, state) do
    log_request "cancel_order"

    nonce = System.system_time(:microsecond) |> Integer.to_string

    url = @api_url <> "/market/cancel?apikey=#{@api_key}&nonce=#{nonce}&uuid=#{ordernumber}"
    headers = [ApiSign: sign(@secret, url), "Content-Type": "application/x-www-form-urlencoded"]

    # IO.inspect body
    order = case PoisonApi.get(url, headers, []) do
      {:ok, body }->
        case body["success"] do
          true -> :ok
          false -> :error
        end
      {:error, _} ->
        :error
    end
    {:reply, order, state}
  end

  def handle_call({:execute_live_trade, currency_pair, rate, amount, type}, _from, state) do
    log_request "execute_live_trade"

    nonce = System.system_time(:microsecond) |> Integer.to_string
    amount = amount |> Float.to_string
    rate = rate |> Float.to_string

    url = @api_url <> "/market/buylimit?apikey=#{@api_key}&nonce=#{nonce}&market=#{currency_pair}&quantity=#{amount}&rate=#{rate}"
    headers = [ApiSign: sign(@secret, url), "Content-Type": "application/x-www-form-urlencoded"]

    IO.inspect url

    order_id = case PoisonApi.get(url, headers, []) do
      {:ok, body }->

        IO.inspect body
        case body["success"] do
          true ->
            result = body["result"]
            order_id = result["uuid"]
            {:ok, order_id}

          false -> :error
        end
      {:error, _} ->
        :error
    end
    {:reply, order_id, state}
  end

  def handle_call({:execute_test_trade, currency_pair, rate, amount, type}, _from, state) do
    trade_time = DateTime.utc_now |> DateTime.to_iso8601
    order_item = %OrderItem{
      order_number: 99999999,
      currency_pair: currency_pair,
      resulting_trades: [%MarketTradeItem{
        amount: amount,
        currency_pair: currency_pair,
        date: trade_time,
        rate: rate,
        total: amount * rate,
        trade_id: "99999",
        type: type
        }]
      }
    IO.puts("Test Trade executed:")
    IO.inspect order_item
    {:reply, order_item, state}
  end

  def handle_call({:get_balance, currency}, _from, state) do
    log_request "get_balance"

    nonce = System.system_time(:microsecond) |> Integer.to_string

    url = @api_url <> "/account/getbalance?apikey=#{@api_key}&nonce=#{nonce}&currency=#{currency}"
    headers = [ApiSign: sign(@secret, url), "Content-Type": "application/x-www-form-urlencoded"]

    IO.inspect url

    balance = case PoisonApi.get(url, headers, []) do
      {:ok, body }->

        IO.inspect body
        case body["success"] do
          true ->
            result = body["result"]
            balance = result["Available"]
            {:ok, balance}

          false -> :error
        end
      {:error, _} ->
        :error
    end
    {:reply, balance, state}
  end

  defp log_request(request) do
    IO.puts "Request made " <> request
    IO.inspect DateTime.utc_now |> DateTime.to_string
  end

  defp sign(key, content) do
    :crypto.hmac(:sha512, key, content)
      |> Base.encode16
  end
end
