defmodule SuperTrader.DateTimeUtils do
  def from_poloniex(date_string) do
    [date , time] = String.split(date_string)

    [year, month, day] = String.split(date, "-")
    [hour, minute, second] = String.split(time, ":")

    datetime = %DateTime{
      year: year |> String.to_integer,
      month: month |> String.to_integer,
      day: day |> String.to_integer,
      hour: hour |> String.to_integer,
      minute: minute |> String.to_integer,
      second: second |> String.to_integer,
      time_zone: "Etc/UTC",
      zone_abbr: "UTC",
      utc_offset: 0,
      std_offset: 0
    }
  end

  def from_bitrex(date_string) do
    [date | _] = date_string |> String.split(".")
    {:ok, date, _} = date <> "Z" |> DateTime.from_iso8601
    date
  end
end
