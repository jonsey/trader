defmodule SuperTrader.TradeManager.Monitor do

  use GenServer

  def create(currency_id) do
    case GenServer.whereis(ref(currency_id)) do
      nil ->
        Supervisor.start_child(SuperTrader.TradeManager.Supervisor, [currency_id])
      _board ->
        {:error, :currency_manager_already_exists}
    end
  end

  def start_link(currency_id) do
    GenServer.start_link(__MODULE__, [], name: ref(currency_id))
  end

  # ...

  defp ref(currency_id) do
    {:global, {:currency_manager, currency_id}}
  end
end
