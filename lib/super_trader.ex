defmodule SuperTrader do
  use Application

  # See http://elixir-lang.org/docs/stable/elixir/Application.html
  # for more information on OTP Applications
  def start(_type, _args) do
    import Supervisor.Spec

    dcr_config = %SuperTrader.TradeManagerConfig{
      limit: 4.0,
      stop: 1.5,
      roc_threshold: 1.0,
      roc_period: 2
    }
    lbc_config = %SuperTrader.TradeManagerConfig{
      limit: 1,
      stop: 0.5,
      roc_threshold: 0.6,
      roc_period: 5
    }
    ardr_config = %SuperTrader.TradeManagerConfig{
      limit: 3,
      stop: 1.5,
      roc_threshold: 1,
      roc_period: 2
    }
    fldc_config = %SuperTrader.TradeManagerConfig{
      limit: 10,
      stop: 1.5,
      roc_threshold: 3,
      roc_period: 2
    }
    sys_config = %SuperTrader.TradeManagerConfig{
      limit: 10,
      stop: 1.5,
      roc_threshold: 1,
      roc_period: 2
    }
    burst_config = %SuperTrader.TradeManagerConfig{
      limit: 10,
      stop: 1.5,
      roc_threshold: 1,
      roc_period: 2
    }
    sc_config = %SuperTrader.TradeManagerConfig{
      limit: 10,
      stop: 1.0,
      roc_threshold: 0.75,
      roc_period: 2
    }
    ltc_config = %SuperTrader.TradeManagerConfig{
      limit: 10,
      stop: 0.5,
      roc_threshold: 0.35,
      roc_period: 5
    }
    flo_config = %SuperTrader.TradeManagerConfig{
      limit: 10,
      stop: 1.5,
      roc_threshold: 1.5,
      roc_period: 2
    }
    steem_config = %SuperTrader.TradeManagerConfig{
      limit: 10,
      stop: 1.5,
      roc_threshold: 1.0,
      roc_period: 2
    }
    xrp_config = %SuperTrader.TradeManagerConfig{
      limit: 10,
      stop: 1.0,
      roc_threshold: 0.5,
      roc_period: 2
    }
    strat_config = %SuperTrader.TradeManagerConfig{
      limit: 2,
      stop: 1.5,
      roc_threshold: 1.5,
      roc_period: 5
    }
    game_config = %SuperTrader.TradeManagerConfig{
      limit: 4,
      stop: 2,
      roc_threshold: 0,
      roc_period: 3
    }
    omni_config = %SuperTrader.TradeManagerConfig{
      limit: 10,
      stop: 1,
      roc_threshold: 1,
      roc_period: 5
    }
    neos_config = %SuperTrader.TradeManagerConfig{
      limit: 10,
      stop: 2,
      roc_threshold: 1.3,
      roc_period: 5
    }
    lsk_config = %SuperTrader.TradeManagerConfig{
      limit: 4,
      stop: 1.5,
      roc_threshold: 1.0,
      roc_period: 5
    }
    vtc_config = %SuperTrader.TradeManagerConfig{
      limit: 4,
      stop: 1.5,
      roc_threshold: 1.0,
      roc_period: 5
    }
    gnt_config = %SuperTrader.TradeManagerConfig{
      limit: 3,
      stop: 1.5,
      roc_threshold: 1.0,
      roc_period: 5
    }

    # Define workers and child supervisors to be supervised
    children = [
      # Start the Ecto repository
      supervisor(SuperTrader.Repo, []),
      # Start the endpoint when the application starts
      supervisor(SuperTrader.Endpoint, []),
      # supervisor(SuperTrader.TradeManager.Supervisor, []),
      # Start your own worker by calling: SuperTrader.Worker.start_link(arg1, arg2, arg3)
      # worker(SuperTrader.Worker, [arg1, arg2, arg3]),
      # worker(SuperTrader.BitrexExchangeApi, [:bitrex_exchange_api], restart: :transient),
      worker(SuperTrader.ExchangeApi, [:exchange_api], restart: :temporary),
      worker(SuperTrader.CapitalManager, [:capital_manager], restart: :temporary),
      worker(SuperTrader.TickerManager, [:ticker_manager], restart: :temporary),
      worker(SuperTrader.TradingMonitor, [:trading_monitor], restart: :temporary),
      worker(SuperTrader.MarketTradeManager, [:market_trade_manager]),
      # worker(SuperTrader.TradeManager, [:trade_manager], restart: :temporary),

      # worker(SuperTrader.PredictionTradeManager, [:BTC_DCR, %{}], id: "DCR", restart: :temporary),
      
      worker(SuperTrader.PredictionTradeManager, [:BTC_DGB, %{}], id: "DGB", restart: :temporary),
      #
      # worker(SuperTrader.PredictionTradeManager, [:BTC_GAME, %{}], id: "GAME", restart: :temporary),
      #
      # worker(SuperTrader.PredictionTradeManager, [:BTC_STRAT, %{}], id: "STRAT", restart: :temporary),
      #
      # worker(SuperTrader.PredictionTradeManager, [:BTC_LBC, %{}], id: "LBC", restart: :temporary),
      #
      # worker(SuperTrader.PredictionTradeManager, [:BTC_OMNI, %{}], id: "OMNI", restart: :temporary),
      #
      # worker(SuperTrader.PredictionTradeManager, [:BTC_NEOS, %{}], id: "NEOS", restart: :temporary),
      #
      # worker(SuperTrader.PredictionTradeManager, [:BTC_LSK, %{}], id: "LSK", restart: :temporary),
      #
      # worker(SuperTrader.PredictionTradeManager, [:BTC_VTC, %{}], id: "VTC", restart: :temporary),
      #
      # worker(SuperTrader.PredictionTradeManager, [:BTC_GNT, %{}], id: "GNT", restart: :temporary),
      #
      # worker(SuperTrader.PredictionTradeManager, [:BTC_XCP, %{}], id: "XCP", restart: :temporary),
      #
      # worker(SuperTrader.PredictionTradeManager, [:BTC_NAUT, %{}], id: "NAUT", restart: :temporary),
      #
      # worker(SuperTrader.PredictionTradeManager, [:BTC_ARDR, %{}], id: "ARDR", restart: :temporary),
      #
      # worker(SuperTrader.PredictionTradeManager, [:BTC_BTS, %{}], id: "BTS", restart: :temporary),
      #
      # worker(SuperTrader.PredictionTradeManager, [:BTC_FLDC, %{}], id: "FLDC", restart: :temporary),
      #
      # worker(SuperTrader.PredictionTradeManager, [:BTC_SYS, %{}], id: "SYS", restart: :temporary),
      #
      # worker(SuperTrader.PredictionTradeManager, [:BTC_BURST, %{}], id: "BURST", restart: :temporary),
      #
      # worker(SuperTrader.PredictionTradeManager, [:BTC_SC, %{}], id: "SC", restart: :temporary),
      #
      # worker(SuperTrader.PredictionTradeManager, [:BTC_LTC, %{}], id: "LTC", restart: :temporary),
      #
      # worker(SuperTrader.PredictionTradeManager, [:BTC_FLO, %{}], id: "FLO", restart: :temporary),
      #
      # worker(SuperTrader.PredictionTradeManager, [:BTC_STEEM, %{}], id: "STEEM", restart: :temporary),
      #
      # worker(SuperTrader.PredictionTradeManager, [:BTC_XRP, %{}], id: "XRP", restart: :temporary),
      #
      # worker(SuperTrader.PredictionTradeManager, [:BTC_CLAM, %{}], id: "CLAM", restart: :temporary),
      #
      # worker(SuperTrader.PredictionTradeManager, [:BTC_ETC, %{}], id: "ETC", restart: :temporary),
      #
      # worker(SuperTrader.PredictionTradeManager, [:BTC_ETH, %{}], id: "ETH", restart: :temporary),
      #
      # worker(SuperTrader.PredictionTradeManager, [:BTC_FCT, %{}], id: "FCT", restart: :temporary),
      #
      # worker(SuperTrader.PredictionTradeManager, [:BTC_XPM, %{}], id: "XPM", restart: :temporary),
      #
      # worker(SuperTrader.PredictionTradeManager, [:BTC_GRC, %{}], id: "GRC", restart: :temporary),
      #
      # worker(SuperTrader.PredictionTradeManager, [:BTC_HUC, %{}], id: "HUC", restart: :temporary),
      #
      # worker(SuperTrader.PredictionTradeManager, [:BTC_MAID, %{}], id: "MAID", restart: :temporary),
      #
      # worker(SuperTrader.PredictionTradeManager, [:BTC_NXT, %{}], id: "NXT", restart: :temporary),
      #
      # worker(SuperTrader.PredictionTradeManager, [:BTC_PINK, %{}], id: "PINK", restart: :temporary),
      #
      # worker(SuperTrader.PredictionTradeManager, [:BTC_VIA, %{}], id: "VIA", restart: :temporary),
      #
      # worker(SuperTrader.PredictionTradeManager, [:BTC_XMP, %{}], id: "XMP", restart: :temporary),
      #
      # worker(SuperTrader.PredictionTradeManager, [:BTC_XEM, %{}], id: "XEM", restart: :temporary),
      #
      # worker(SuperTrader.PredictionTradeManager, [:BTC_ZEC, %{}], id: "ZEC", restart: :temporary),


#       worker(SuperTrader.BittrexTradeManager, [
#         "BTC-DCR,
# ]),

    ]

    # See http://elixir-lang.org/docs/stable/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: SuperTrader.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    SuperTrader.Endpoint.config_change(changed, removed)
    :ok
  end
end
